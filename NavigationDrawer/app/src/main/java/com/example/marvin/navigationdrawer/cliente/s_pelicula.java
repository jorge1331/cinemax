package com.example.marvin.navigationdrawer.cliente;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonPeliculas;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link s_pelicula.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link s_pelicula#newInstance} factory method to
 * create an instance of this fragment.
 */
public class s_pelicula extends Fragment {
    View v;
    ListView lvPeliculasV;
    Button btnAtrasLugar;
    Button btnSiguienteLugar;
    Button btnCancelarLugar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_s_pelicula, container, false);
        lvPeliculasV = (ListView)v.findViewById(R.id.lvPeliculasSele);
        btnAtrasLugar = (Button)v.findViewById(R.id.btnAtrasLugar);
        btnSiguienteLugar = (Button)v.findViewById(R.id.btnSiguienteLugar);
        btnCancelarLugar = (Button)v.findViewById(R.id.btnCancelarLugar);

        CargaData();

        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnCancelarLugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new Principal();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });

        btnSiguienteLugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new r_lugar();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });

        btnAtrasLugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new r_hora();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });
    }

    public void CargaData()
    {


    }

    public void PantPrincipal()
    {
        Fragment newFragment = new Principal();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }
}
