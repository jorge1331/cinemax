package com.example.marvin.navigationdrawer.Web;

import com.android.volley.Request;

/**
 * Created by DELL on 27/04/2017.
 */

public interface BaseBolleyFragment {

        public void onCreate();
        public void onStop();
        public  void addToQueue(Request request);
        public  void onPreStartConnection();
        public  void onConnectionFinished();
        public  void onConnectionFailed(String error);


}
