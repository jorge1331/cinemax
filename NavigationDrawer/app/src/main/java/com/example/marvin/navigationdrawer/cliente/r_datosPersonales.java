package com.example.marvin.navigationdrawer.cliente;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link r_datosPersonales.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class r_datosPersonales extends Fragment {
Button btnCancelar;
Button btnSiguiente;
View view;
    private OnFragmentInteractionListener mListener;

    public r_datosPersonales() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_r_datos_personales, container, false);

        btnCancelar =(Button)view.findViewById(R.id.btnCancelar);
        btnSiguiente = (Button)view.findViewById(R.id.btnSiguiente);
        return  view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new Principal();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new r_detalle();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });

    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
