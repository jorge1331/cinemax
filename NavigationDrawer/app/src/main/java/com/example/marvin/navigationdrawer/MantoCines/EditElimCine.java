package com.example.marvin.navigationdrawer.MantoCines;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonCines;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static android.R.attr.name;
import static com.example.marvin.navigationdrawer.R.drawable.cine;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditElimCine extends Fragment
{
    Bundle captura;
    View v;
    private Gson gson = new Gson();
    Cine cine;
    List<String> allCines;
    int idCine = 0;
    EditText nomCine, dirCine, canSalCine, latCine, lonCine;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_edit_elim_cine, container, false);
        return v;
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        //Conseguimos el nombre seleccionado en el listview.. con este consultaremos la BD
        captura = getArguments();
        String nombre = captura.getString("NombreCine");


        //llamo al metodo que me conseguira el registro con todos los campos
        conseguirCine(nombre);


        //evaluamos que boton a sido pulsado

        Button BtnEditar = (Button)v.findViewById(R.id.btnEditarCine);
        BtnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnEditar();
            }
        });

        Button BtnBorrar = (Button)v.findViewById(R.id.btnBorrarCine);
        BtnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BorrarCine();

            }
        });

        Button BtnAtras = (Button)v.findViewById(R.id.btnAtrasCine);
        BtnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(3);
            }
        });

        Button BtnGuardar = (Button)v.findViewById(R.id.btnGuardarEditCine);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                EditarCine();
                PantPrincipal(4);
            }
        });

    }



    private void conseguirCine(String nombre_cine) {

        String url = Constantes.obtener_cines_filtrado+"?nombre_cine="+nombre_cine+"";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                procesarRespuestaDetail(response);
                onConnectionFinished();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                onConnectionFailed(volleyError.toString());
            }
        });
        addToQueue(request);

    }

    private void procesarRespuestaDetail(JSONObject response) {
        try {
            // Obtener atributo "estado"
            String estado = response.getString("estado");


            switch (estado) {
                case "1": // EXITO
                    // Obtener el array "usuario"
                    JSONObject object = response.getJSONObject("cine");
                    // Asignamos el resultado del JSON a nuestro modelo Usuarios
                    cine = gson.fromJson(object.toString(), Cine.class);

                    nomCine.setText(cine.getNombre_cine());
                    dirCine.setText(cine.getDireccion());
                    canSalCine.setText(String.valueOf(cine.getNumeroSalas()));
                    latCine.setText(String.valueOf(cine.getLatitud()));
                    lonCine.setText(String.valueOf(cine.getLongitud()));

                    break;
                case "2": // FALLIDO
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            getActivity(),
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {

        }

    }

    
    public void EditarCine()
    {
        int cod = idCine;

        nomCine = (EditText)v.findViewById(R.id.edtNombreCine);
        dirCine = (EditText)v.findViewById(R.id.edtDireccionCine);
        lonCine = (EditText)v.findViewById(R.id.edtLongitud);
        latCine = (EditText)v.findViewById(R.id.edtLatitud);

        if(TextUtils.isEmpty(nomCine.getText()))
        {
            nomCine.setFocusable(true);
            nomCine.setError("Ingrese cine");
        }
        else if(TextUtils.isEmpty(dirCine.getText()))
        {
            dirCine.setFocusable(true);
            dirCine.setError("Ingrese Direccion");
        }
        else if(TextUtils.isEmpty(latCine.getText()))
        {
            latCine.setFocusable(true);
            latCine.setError("Ingrese Latitud");
        }
        else if(TextUtils.isEmpty(lonCine.getText()))
        {
            lonCine.setFocusable(true);
            lonCine.setError("Ingrese Longitud");
        }
        else
        {
            AdmonCines Cin = new AdmonCines();

            //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
            Cin.setCine_id(cod);
            Cin.setNombreCine(nomCine.getText().toString());
            Cin.setDireccionCine(dirCine.getText().toString());
            Cin.setLatitud(Integer.parseInt(latCine.getText().toString()));
            Cin.setLongitud(Integer.parseInt(lonCine.getText().toString()));

            int Resp = Cin.EditarCine(Cin);


            if(Resp == 1)
            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                PantPrincipal(1);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void BorrarCine()
    {
        int cod = idCine;

        AdmonCines Cin = new AdmonCines();

        //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
        Cin.setCine_id(cod);

        int Resp = Cin.EliminarCine(Cin);


        if(Resp == 1)
        {
            Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
            PantPrincipal(1);
        }
        else
        {
            Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
        }
    }


    //Metodo para llamar un nuevo fragment al pulsar algun boton.
    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new Principal();
        if(FragementEle == 1)
        {
            newFragment = new admonCines();


        }else if(FragementEle == 3)
        {
            newFragment = new admonCines();

        }else if(FragementEle == 4)
        {
            newFragment = new admonCines();

        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }


    //Metodo para habilitar los campos bloqueados y mostrar los ocultos para editar el campo.
    public void btnEditar()
    {
        //Asignando a cada variable un objeto de la vista a minupular
        EditText nombre = (EditText)v.findViewById(R.id.edtNombreCine);
        EditText direccion = (EditText)v.findViewById(R.id.edtDireccionCine);
        EditText latitud = (EditText)v.findViewById(R.id.edtLatitud);
        EditText longitud = (EditText)v.findViewById(R.id.edtLongitud);
        Button guardar = (Button)v.findViewById(R.id.btnGuardarEditCine);
        Button editar = (Button)v.findViewById(R.id.btnEditarCine);
        Button eliminar = (Button) v.findViewById(R.id.btnBorrarCine);

        //Al dar click en Editar los EditText se habilitaran para edicion
        nombre.setEnabled(true);
        latitud.setEnabled(true);
        direccion.setEnabled(true);
        longitud.setEnabled(true);

        //Algunos botones se muestran y otros se ocultan
        guardar.setVisibility(View.VISIBLE);
        editar.setVisibility(View.INVISIBLE);
        eliminar.setVisibility(View.INVISIBLE);

    }

    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;




    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

}
