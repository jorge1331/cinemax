/**
 * Created by Marvin on 15/03/2017.
 */
package com.example.marvin.navigationdrawer;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonCines;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link reVentas.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link reVentas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class reVentas extends Fragment {
    View v;
    ListView lvCinesV;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_re_ventas, container, false);
        lvCinesV = (ListView)v.findViewById(R.id.lvCinesVentas);
        CargaData();
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button Btn = (Button)v.findViewById(R.id.btnPrincipalReVentas);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal();
            }
        });

    }

    public void CargaData()
    {
        AdmonCines Cin = new AdmonCines();
        List<String> allCines= Cin.SelectCinesVentas(Cin);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allCines);

        lvCinesV.setAdapter(adaptador);
    }

    public void PantPrincipal()
    {
        Fragment newFragment = new Principal();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }
}
