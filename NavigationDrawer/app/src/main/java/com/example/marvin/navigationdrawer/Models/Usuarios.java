package com.example.marvin.navigationdrawer.Models;

/**
 * Created by DELL on 21/05/2017.
 */

public class Usuarios {

    public  String appelido_usuario;
    public  String cargoUsuario_id;
    public String celular_usuario;
    public  String clave_usuario;
    public  String correo_usuario;
    public  String direccion_usuario;
    public  String dui_usuario;
    public int edad_usuario;
    public  String feccha_nac_usuario;
    public  int id_usuario;
    public  int isActive;
    public  String nombre_usuario;
    public  String telefono_usuario;
    public  String usuario_usuario;


    public String getAppelido_usuario() {
        return appelido_usuario;
    }

    public void setAppelido_usuario(String appelido_usuario) {
        this.appelido_usuario = appelido_usuario;
    }

    public String getCargoUsuario_id() {
        return cargoUsuario_id;
    }

    public void setCargoUsuario_id(String cargoUsuario_id) {
        this.cargoUsuario_id = cargoUsuario_id;
    }

    public String getCelular_usuario() {
        return celular_usuario;
    }

    public void setCelular_usuario(String celular_usuario) {
        this.celular_usuario = celular_usuario;
    }

    public String getClave_usuario() {
        return clave_usuario;
    }

    public void setClave_usuario(String clave_usuario) {
        this.clave_usuario = clave_usuario;
    }

    public String getCorreo_usuario() {
        return correo_usuario;
    }

    public void setCorreo_usuario(String correo_usuario) {
        this.correo_usuario = correo_usuario;
    }

    public String getDireccion_usuario() {
        return direccion_usuario;
    }

    public void setDireccion_usuario(String direccion_usuario) {
        this.direccion_usuario = direccion_usuario;
    }

    public String getDui_usuario() {
        return dui_usuario;
    }

    public void setDui_usuario(String dui_usuario) {
        this.dui_usuario = dui_usuario;
    }

    public int getEdad_usuario() {
        return edad_usuario;
    }

    public void setEdad_usuario(int edad_usuario) {
        this.edad_usuario = edad_usuario;
    }

    public String getFeccha_nac_usuario() {
        return feccha_nac_usuario;
    }

    public void setFeccha_nac_usuario(String feccha_nac_usuario) {
        this.feccha_nac_usuario = feccha_nac_usuario;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getTelefono_usuario() {
        return telefono_usuario;
    }

    public void setTelefono_usuario(String telefono_usuario) {
        this.telefono_usuario = telefono_usuario;
    }

    public String getUsuario_usuario() {
        return usuario_usuario;
    }

    public void setUsuario_usuario(String usuario_usuario) {
        this.usuario_usuario = usuario_usuario;
    }
}
