package com.example.marvin.navigationdrawer.MantoSalas;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonSalas;
import com.example.marvin.navigationdrawer.MantoUsuarios.infoUsuario;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link admon_salas.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link admon_salas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class admon_salas extends Fragment
{

    View v;
    ListView lvListarSala;
    EditText buscarSala;
    ArrayAdapter<String> adaptador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_admon_salas, container, false);
        lvListarSala = (ListView)v.findViewById(R.id.lvListarSala);
        CargaData();
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        ImageButton BtnBuscar = (ImageButton) v.findViewById(R.id.btnBuscarSala);
        BtnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BuscarSala();
            }
        });

        Button BtnAgregar = (Button)v.findViewById(R.id.btnAgregarSala);
        BtnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(2);
            }
        });

        final Button BtnMostrarTodo = (Button)v.findViewById(R.id.btnMostrarTodosSala);
        BtnMostrarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CargaData();
                BtnMostrarTodo.setVisibility(View.INVISIBLE);
            }
        });


        lvListarSala.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                //Consigo el nombre del item de la lista selecionada
                String nombreSala = lvListarSala.getItemAtPosition(position).toString();

                //mMando a inicializar el nuevo fragment
                Fragment newFragment = new EditElimSala();


                //Envio el nombre conseguido del usario seleccionado en la lista
                Bundle args = new Bundle();
                args.putString("NombreSala", nombreSala);
                newFragment.setArguments(args);

                //Cambio de fragment
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();

            }
        });

    }


    public void BuscarSala()
    {
        //Capturo lo escrito en el edittext buscar
        buscarSala = (EditText) v.findViewById(R.id.edtBuscarSala);

        //Evaluo si lo ingresado por el usuario es texto en blanco
        if(TextUtils.isEmpty(buscarSala.getText())) {}
        else
        {
            //Creo Objeto de la clase que se comunica con la bd
            AdmonSalas Sal = new AdmonSalas(getActivity());

            //Asigno lo capturado a la variable nombre de la clase que se comunica con la bd
            //por medio del set que luego en el metodo interno de la clase referenciada
            //se usara para la consulta
            Sal.setNombreSala(buscarSala.getText().toString());

            //Obtengo el resultado de la consulta por nombre
            List<String> allSalas= Sal.SearchSala(Sal);

            //Evaluo si el resultado esta vacio, lo que significaria que no exite registro
            //con ese nombre
            if (allSalas.isEmpty())
            {
                Toast.makeText(getActivity(),"Sin Coincidencias",Toast.LENGTH_SHORT).show();
                CargaData();
            }else
            {
                //Limpio el adaptador del listview para volverlo a llenar con el resultado
                //de la busqueda y luego vuelvo a llenar el adaptador y se lo mando al listview
                adaptador.clear();
                adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allSalas);
                lvListarSala.setAdapter(adaptador);

                Button todos = (Button) v.findViewById(R.id.btnMostrarTodosSala);
                todos.setVisibility(View.VISIBLE);

            }


        }
    }



    public void CargaData()
    {
        AdmonSalas Sal = new AdmonSalas(getActivity());
        List<String> allSalas= Sal.SelectSalas(Sal);

        adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allSalas);

        lvListarSala.setAdapter(adaptador);
    }

    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new Principal();
        if(FragementEle == 1)
        {
            newFragment = new infoUsuario();
        }else if(FragementEle == 2)
        {
            newFragment = new rSalas();
        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }


}