package com.example.marvin.navigationdrawer.Models;

/**
 * Created by DELL on 21/05/2017.
 */

public class Salas {

    public  int id_cine_fk;
    public  int id_salas;
    public int isActive;
    public  String nombre_salas;
    public  int numero_butacas;

    public int getId_cine_fk() {
        return id_cine_fk;
    }

    public void setId_cine_fk(int id_cine_fk) {
        this.id_cine_fk = id_cine_fk;
    }

    public int getId_salas() {
        return id_salas;
    }

    public void setId_salas(int id_salas) {
        this.id_salas = id_salas;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getNombre_salas() {
        return nombre_salas;
    }

    public void setNombre_salas(String nombre_salas) {
        this.nombre_salas = nombre_salas;
    }

    public int getNumero_butacas() {
        return numero_butacas;
    }

    public void setNumero_butacas(int numero_butacas) {
        this.numero_butacas = numero_butacas;
    }
}
