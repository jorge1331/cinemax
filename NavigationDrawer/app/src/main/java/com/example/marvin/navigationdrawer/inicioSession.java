package com.example.marvin.navigationdrawer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link inicioSession.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link inicioSession#newInstance} factory method to
 * create an instance of this fragment.
 */
public class inicioSession extends Fragment {
    private Context globalContext = null;

    EditText edtNombre, edtPassword;
    View v;
    Button BtnGuardar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_inicio_session, container, false);
        BtnGuardar = (Button)v.findViewById(R.id.btnIngresarInfo);
        return v;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GuardarCine();
            }
        });
    }


    public void GuardarCine(){
        edtNombre = (EditText)v.findViewById(R.id.edtUsuarioLog);
        edtPassword = (EditText)v.findViewById(R.id.edtPasswordLog);
        NavigationView nav;
        // Lookup navigation view



        if(TextUtils.isEmpty(edtNombre.getText()))
        {
            Toast.makeText(getActivity(), "Escriba el Usuario", Toast.LENGTH_LONG).show();
            edtNombre.setFocusable(true);
        }
        if(TextUtils.isEmpty(edtNombre.getText()))
        {
            Toast.makeText(getActivity(), "Escriba el Password", Toast.LENGTH_LONG).show();
            edtPassword.setFocusable(true);
        }
        else
        {
            Intent obj = new Intent(getActivity(), MainActivity.class);
            obj.putExtra("Valor", "Admin");
            startActivity(obj);
        }
    }

    public void PantPrincipal()
    {
        Fragment newFragment = new Principal();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }
}
