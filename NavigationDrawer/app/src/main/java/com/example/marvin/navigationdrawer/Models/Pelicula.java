package com.example.marvin.navigationdrawer.Models;

/**
 * Created by DELL on 21/05/2017.
 */

public class Pelicula {
    public  int ano_pelicula;
    public  String clasificacion_pelicula;
    public  String director_pelicula;
    public  String escritor_pelicula;
    public  String genero_pelicula;
    public  int id_pelicula;
    public  int id_pelicula_tipo_fk;
    public  int isActive;
    public  String nombre_pelicula;
    public  String productora_pelicula;
    public  String sipnosis_pelicula;

    public int getAno_pelicula() {
        return ano_pelicula;
    }

    public void setAno_pelicula(int ano_pelicula) {
        this.ano_pelicula = ano_pelicula;
    }

    public String getClasificacion_pelicula() {
        return clasificacion_pelicula;
    }

    public void setClasificacion_pelicula(String clasificacion_pelicula) {
        this.clasificacion_pelicula = clasificacion_pelicula;
    }

    public String getDirector_pelicula() {
        return director_pelicula;
    }

    public void setDirector_pelicula(String director_pelicula) {
        this.director_pelicula = director_pelicula;
    }

    public String getGenero_pelicula() {
        return genero_pelicula;
    }

    public void setGenero_pelicula(String genero_pelicula) {
        this.genero_pelicula = genero_pelicula;
    }

    public String getEscritor_pelicula() {
        return escritor_pelicula;
    }

    public void setEscritor_pelicula(String escritor_pelicula) {
        this.escritor_pelicula = escritor_pelicula;
    }

    public int getId_pelicula() {
        return id_pelicula;
    }

    public void setId_pelicula(int id_pelicula) {
        this.id_pelicula = id_pelicula;
    }

    public int getId_pelicula_tipo_fk() {
        return id_pelicula_tipo_fk;
    }

    public void setId_pelicula_tipo_fk(int id_pelicula_tipo_fk) {
        this.id_pelicula_tipo_fk = id_pelicula_tipo_fk;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getNombre_pelicula() {
        return nombre_pelicula;
    }

    public void setNombre_pelicula(String nombre_pelicula) {
        this.nombre_pelicula = nombre_pelicula;
    }

    public String getProductora_pelicula() {
        return productora_pelicula;
    }

    public void setProductora_pelicula(String productora_pelicula) {
        this.productora_pelicula = productora_pelicula;
    }

    public String getSipnosis_pelicula() {
        return sipnosis_pelicula;
    }

    public void setSipnosis_pelicula(String sipnosis_pelicula) {
        this.sipnosis_pelicula = sipnosis_pelicula;
    }
}
