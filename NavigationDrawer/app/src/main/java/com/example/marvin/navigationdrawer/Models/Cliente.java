package com.example.marvin.navigationdrawer.Models;

/**
 * Created by DELL on 21/05/2017.
 */

public class Cliente {
    public  String apellido_cliente;
    public  String correo_cliente;
    public  String dui_cliente;
    public  int edad_cliente;
    public  int id_cliente;
    public  String nombre_cliente;

    public String getApellido_cliente() {
        return apellido_cliente;
    }

    public void setApellido_cliente(String apellido_cliente) {
        this.apellido_cliente = apellido_cliente;
    }

    public String getCorreo_cliente() {
        return correo_cliente;
    }

    public void setCorreo_cliente(String correo_cliente) {
        this.correo_cliente = correo_cliente;
    }

    public String getDui_cliente() {
        return dui_cliente;
    }

    public void setDui_cliente(String dui_cliente) {
        this.dui_cliente = dui_cliente;
    }

    public int getEdad_cliente() {
        return edad_cliente;
    }

    public void setEdad_cliente(int edad_cliente) {
        this.edad_cliente = edad_cliente;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }
}
