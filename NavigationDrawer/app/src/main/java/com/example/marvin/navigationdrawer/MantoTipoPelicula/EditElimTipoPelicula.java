package com.example.marvin.navigationdrawer.MantoTipoPelicula;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonTipoPelicula;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditElimTipoPelicula.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditElimTipoPelicula#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditElimTipoPelicula extends Fragment
{

    Bundle captura;
    View v;
    int idTP = 0;
    EditText tipoPelicula, descripcionPelicula, precioEntradaTP;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_edit_elim_tipo_pelicula, container, false);
        return v;

    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        //Conseguimos el nombre seleccionado en el listview.. con este consultaremos la BD
        captura = getArguments();
        String nombre = captura.getString("tipoPelicula");


        //llamo al metodo que me conseguira el registro con todos los campos
        conseguirTipoPelicula(nombre);


        //evaluamos que boton a sido pulsado

        Button BtnEditar = (Button)v.findViewById(R.id.btnEditarTp);
        BtnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnEditar();
            }
        });

        Button BtnBorrar = (Button)v.findViewById(R.id.btnBorrarTp);
        BtnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BorrarTipoPelicula();

            }
        });

        Button BtnAtras = (Button)v.findViewById(R.id.btnAtrasTp);
        BtnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(3);
            }
        });

        Button BtnGuardar = (Button)v.findViewById(R.id.btnGuardarEditTp);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditarTipoPelicula();
                PantPrincipal(4);
            }
        });

    }


    public void conseguirTipoPelicula (String nombre)
    {

        String tp = "", descripciontp = "";
        double preciotp = 0.0;


        //Creo Objeto de la clase que se comunica con la bd
        AdmonTipoPelicula Tp = new AdmonTipoPelicula(getActivity());

        //Asigno lo capturado a la variable nombre de la clase que se comunica con la bd
        //por medio del set que luego en el metodo interno de la clase referenciada
        //se usara para la consulta
        Tp.setTipoPelicula(nombre);

        //Inicializando un cursor
        Cursor c = Tp.DatosAEditarTipoPelicula(Tp);

        c.moveToFirst();

        //Asigno cada campo del registro a variables, para luego ser utilizadas
        if (c.moveToFirst())
        {
            do {
                 idTP= c.getInt(0);
                tp = c.getString(1);
                descripciontp = c.getString(2);
                preciotp = c.getDouble(3);

            }while(c.moveToNext());
        }


        //Haciendo Cast de los objetos a utilizar
        tipoPelicula = (EditText)v.findViewById(R.id.edtNombreTP);
        descripcionPelicula = (EditText)v.findViewById(R.id.edtDescripcionTP);
        precioEntradaTP = (EditText)v.findViewById(R.id.edtPrecioEntradaTP);

        tipoPelicula.setText(tp);
        descripcionPelicula.setText(descripciontp);
        precioEntradaTP.setText(String.valueOf(preciotp));

    }


    public void EditarTipoPelicula()
    {
        int cod = idTP;

        tipoPelicula = (EditText)v.findViewById(R.id.edtNombreTP);
        descripcionPelicula = (EditText)v.findViewById(R.id.edtDescripcionTP);
        precioEntradaTP = (EditText)v.findViewById(R.id.edtPrecioEntradaTP);

        if(TextUtils.isEmpty(tipoPelicula.getText()))
        {
            tipoPelicula.setFocusable(true);
            tipoPelicula.setError("Ingrese el nombre del Tipo de Pelicula");
        }
        else if(TextUtils.isEmpty(descripcionPelicula.getText()))
        {
            descripcionPelicula.setFocusable(true);
            descripcionPelicula.setError("Ingrese una breve descripcion");
        }
        else if(TextUtils.isEmpty(precioEntradaTP.getText()))
        {
            precioEntradaTP.setFocusable(true);
            precioEntradaTP.setError("Ingrese el precio de la entrada para este tipo de pelicula");
        }
        else
        {
            AdmonTipoPelicula Tp = new AdmonTipoPelicula(getActivity());

            //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
            Tp.setIdPeliculaTipo(cod);
            Tp.setTipoPelicula(tipoPelicula.getText().toString());
            Tp.setDescripcionTipoPelicula(descripcionPelicula.getText().toString());
            Tp.setPrecioTipoPelicula(Double.parseDouble(precioEntradaTP.getText().toString()));

            int Resp = Tp.EditarTipoPelicula(Tp);


            if(Resp == 1)
            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                PantPrincipal(1);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void BorrarTipoPelicula()
    {
        int cod = idTP;

        AdmonTipoPelicula Tp = new AdmonTipoPelicula(getActivity());

        //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
        Tp.setIdPeliculaTipo(cod);

        int Resp = Tp.EliminarTipoPelicula(Tp);


        if(Resp == 1)
        {
            Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
            PantPrincipal(1);
        }
        else
        {
            Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
        }
    }


    //Metodo para llamar un nuevo fragment al pulsar algun boton.
    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new Principal();
        if(FragementEle == 1)
        {
            newFragment = new admonTipoPelicula();


        }else if(FragementEle == 3)
        {
            newFragment = new admonTipoPelicula();

        }else if(FragementEle == 4)
        {
            newFragment = new admonTipoPelicula();

        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }


    //Metodo para habilitar los campos bloqueados y mostrar los ocultos para editar el campo.
    public void btnEditar()
    {
        //Asignando a cada variable un objeto de la vista a minupular
        tipoPelicula = (EditText)v.findViewById(R.id.edtNombreTP);
        descripcionPelicula = (EditText)v.findViewById(R.id.edtDescripcionTP);
        precioEntradaTP = (EditText)v.findViewById(R.id.edtPrecioEntradaTP);
        Button guardar = (Button)v.findViewById(R.id.btnGuardarEditTp);
        Button editar = (Button)v.findViewById(R.id.btnEditarTp);
        Button eliminar = (Button) v.findViewById(R.id.btnBorrarTp);

        //Al dar click en Editar los EditText se habilitaran para edicion
        tipoPelicula.setEnabled(true);
        descripcionPelicula.setEnabled(true);
        precioEntradaTP.setEnabled(true);

        //Algunos botones se muestran y otros se ocultan
        guardar.setVisibility(View.VISIBLE);
        editar.setVisibility(View.INVISIBLE);
        eliminar.setVisibility(View.INVISIBLE);

    }

}
