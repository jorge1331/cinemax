package com.example.marvin.navigationdrawer.MantoUsuarios;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonUsuarios;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditElimUsuario extends Fragment
{
    Bundle captura;
    View v;
    int id_usuario = 0;
    EditText nombreUsuario, apellidoUsuario, direccionUsuario, correoUsuario, usuarioUsuario, claveUsuario;
    EditText telefonoUsuario, duiUsuario;
    Spinner cargo_Usuario;
    int cUsu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_edit_elim_usuario, container, false);
        LlenarSpCargo();
        return v;

    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        //Conseguimos el nombre seleccionado en el listview.. con este consultaremos la BD
        captura = getArguments();
        String nombre = captura.getString("NombreUsuario");


        //llamo al metodo que me conseguira el registro con todos los campos
        ConseguirUsuario(nombre);

        //evaluamos que boton a sido pulsado

        Button BtnEditar = (Button)v.findViewById(R.id.btnEditarUsuario);
        BtnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnEditar();
            }
        });

        Button BtnBorrar = (Button)v.findViewById(R.id.btnBorrarUsuario);
        BtnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BorrarUsuario();

            }
        });

        Button BtnAtras = (Button)v.findViewById(R.id.btnAtrasUsuarioR);
        BtnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(3);
            }
        });

        Button BtnGuardar = (Button)v.findViewById(R.id.btnGuardarUsuario);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(4);
                EditarUsuario();
            }
        });



        //Saber que Item fue selecionado en cargo usuario

        cargo_Usuario= (Spinner) v.findViewById(R.id.spCargoUsuario);

        cargo_Usuario.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l)
            {
                cUsu = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {
                cUsu = 0;
            }
        });

    }

    public void LlenarSpCargo()
    {
         cargo_Usuario= (Spinner) v.findViewById(R.id.spCargoUsuario);

        String[] cargos = getResources().getStringArray(R.array.CargoUsuariosArray);

        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1,cargos);

        cargo_Usuario.setAdapter(adapter);


    }


    //Este metodo conseguira todos los registros para poderlos editar por el usuario
    public void ConseguirUsuario (String nombre)
    {

        String nombreUsu = "", apellidoUsu = "", direccionUsu = "", correoUsu = "", duiUsua = "", usuarioUsu = "", claveUsu = "";
        int  telefonoUsu = 0, idUsua = 0;


        //Creo Objeto de la clase que se comunica con la bd
        AdmonUsuarios Usu = new AdmonUsuarios(getActivity());

        //Asigno lo capturado a la variable nombre de la clase que se comunica con la bd
        //por medio del set que luego en el metodo interno de la clase referenciada
        //se usara para la consulta
        Usu.setNombreUsuario(nombre);

        //Inicializando un cursor
        Cursor c = Usu.DatosAEditarUsuario(Usu);

        c.moveToFirst();

        //Asigno cada campo del registro a variables, para luego ser utilizadas
        if (c.moveToFirst())
        {
            do {
                idUsua = c.getInt(0);
                nombreUsu = c.getString(1);
                apellidoUsu = c.getString(2);
                direccionUsu = c.getString(3);
                duiUsua = c.getString(4);
                telefonoUsu = c.getInt(5);
                correoUsu = c.getString(6);
                usuarioUsu = c.getString(7);
                claveUsu = c.getString(8);

            }while(c.moveToNext());
        }

        id_usuario = idUsua;

        //Haciendo Cast de los objetos a utilizar
        nombreUsuario = (EditText) v.findViewById(R.id.edtNombreUsuario);
        apellidoUsuario = (EditText) v.findViewById(R.id.edtApellidoUsuario);
        direccionUsuario = (EditText) v.findViewById(R.id.edtDireccionUsuario);
        telefonoUsuario = (EditText) v.findViewById(R.id.edtTelefonoUsuario);
        correoUsuario = (EditText) v.findViewById(R.id.edtCorroUsuario);
        duiUsuario = (EditText) v.findViewById(R.id.edtDuiUsuario);
        usuarioUsuario = (EditText) v.findViewById(R.id.edtUsuarioUsuario);
        claveUsuario = (EditText) v.findViewById(R.id.edtPasswordUsuario);

        nombreUsuario.setText(nombreUsu);
        apellidoUsuario.setText(apellidoUsu);
        duiUsuario.setText(duiUsua);
        correoUsuario.setText(correoUsu);
        direccionUsuario.setText(direccionUsu);
        telefonoUsuario.setText(String.valueOf(telefonoUsu));
        usuarioUsuario.setText(usuarioUsu);
        claveUsuario.setText(claveUsu);

    }


    public void EditarUsuario()
    {
        int cod = id_usuario;

        nombreUsuario = (EditText) v.findViewById(R.id.edtNombreUsuario);
        apellidoUsuario = (EditText) v.findViewById(R.id.edtApellidoUsuario);
        direccionUsuario = (EditText) v.findViewById(R.id.edtDireccionUsuario);
        telefonoUsuario = (EditText) v.findViewById(R.id.edtTelefonoUsuario);
        correoUsuario = (EditText) v.findViewById(R.id.edtCorroUsuario);
        duiUsuario = (EditText) v.findViewById(R.id.edtDuiUsuario);
        usuarioUsuario = (EditText) v.findViewById(R.id.edtUsuarioUsuario);
        claveUsuario = (EditText) v.findViewById(R.id.edtPasswordUsuario);


        if((TextUtils.isEmpty(nombreUsuario.getText())) || (TextUtils.isEmpty(apellidoUsuario.getText())) || (TextUtils.isEmpty(direccionUsuario.getText())))
        {
            Toast.makeText(getActivity(), "Rellene todos los campos", Toast.LENGTH_LONG).show();
        }
        else if((TextUtils.isEmpty(duiUsuario.getText())) || (TextUtils.isEmpty(telefonoUsuario.getText())) || (TextUtils.isEmpty(usuarioUsuario.getText())))
        {
            Toast.makeText(getActivity(), "Rellene todos los campos", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(claveUsuario.getText()))
        {
            Toast.makeText(getActivity(), "Rellene todos los campos", Toast.LENGTH_LONG).show();
        }
        else
        {
            AdmonUsuarios usu = new AdmonUsuarios(getActivity());

            usu.setIdUsuario(cod);
            usu.setNombreUsuario(nombreUsuario.getText().toString());
            usu.setApellidoUsuario(apellidoUsuario.getText().toString());
            usu.setDireccionUsuario(direccionUsuario.getText().toString());
            usu.setTelefonoUsuario(Integer.parseInt(telefonoUsuario.getText().toString()));
            usu.setCorreoUsuario(correoUsuario.getText().toString());
            usu.setDuiUsuario(Integer.parseInt(duiUsuario.getText().toString()));
            usu.setUsuarioUsuario(usuarioUsuario.getText().toString());
            usu.setClaveUsuario(claveUsuario.getText().toString());

            int Resp = usu.EditarUsuarios(usu);


            if(Resp == 1)
            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                PantPrincipal(1);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void BorrarUsuario()
    {
        int cod = id_usuario;

        AdmonUsuarios Usu = new AdmonUsuarios(getActivity());

        //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
        Usu.setIdUsuario(cod);

        int Resp = Usu.EliminarUsuario(Usu);


        if(Resp == 1)
        {
            Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
            PantPrincipal(1);
        }
        else
        {
            Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
        }
    }


    //Metodo para llamar un nuevo fragment al pulsar algun boton.
    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new Principal();
        if(FragementEle == 1)
        {
            newFragment = new admonUsuarios();

        }else if(FragementEle == 3)
        {
            newFragment = new admonUsuarios();

        }else if(FragementEle == 4)
        {
            newFragment = new admonUsuarios();

        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }


    //Metodo para habilitar los campos bloqueados y mostrar los ocultos para editar el campo.
    public void btnEditar()
    {
        //Asignando a cada variable un objeto de la vista a minupular
        nombreUsuario = (EditText) v.findViewById(R.id.edtNombreUsuario);
        apellidoUsuario = (EditText) v.findViewById(R.id.edtApellidoUsuario);
        direccionUsuario = (EditText) v.findViewById(R.id.edtDireccionUsuario);
        telefonoUsuario = (EditText) v.findViewById(R.id.edtTelefonoUsuario);
        correoUsuario = (EditText) v.findViewById(R.id.edtCorroUsuario);
        duiUsuario = (EditText) v.findViewById(R.id.edtDuiUsuario);
        usuarioUsuario = (EditText) v.findViewById(R.id.edtUsuarioUsuario);
        claveUsuario = (EditText) v.findViewById(R.id.edtPasswordUsuario);

        cargo_Usuario = (Spinner)v.findViewById(R.id.spCargoUsuario);

        Button guardar = (Button)v.findViewById(R.id.btnGuardarUsuario);
        Button editar = (Button)v.findViewById(R.id.btnEditarUsuario);
        Button eliminar = (Button) v.findViewById(R.id.btnBorrarUsuario);

        //Al dar click en Editar los EditText se habilitaran para edicion
        nombreUsuario.setEnabled(true);
        apellidoUsuario.setEnabled(true);
        direccionUsuario.setEnabled(true);
        telefonoUsuario.setEnabled(true);
        correoUsuario.setEnabled(true);
        duiUsuario.setEnabled(true);
        usuarioUsuario.setEnabled(true);
        claveUsuario.setEnabled(true);
        cargo_Usuario.setEnabled(true);

        //Algunos botones se muestran y otros se ocultan
        cargo_Usuario.setVisibility(View.VISIBLE);
        guardar.setVisibility(View.VISIBLE);
        editar.setVisibility(View.INVISIBLE);
        eliminar.setVisibility(View.INVISIBLE);







    }

}
