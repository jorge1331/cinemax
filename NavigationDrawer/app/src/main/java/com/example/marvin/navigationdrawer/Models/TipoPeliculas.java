package com.example.marvin.navigationdrawer.Models;

/**
 * Created by DELL on 21/05/2017.
 */

public class TipoPeliculas {
    public  int id_tipo_pelicula;
    public  int id_precio_entrada_fk;
    public  int isActive;
    public  String tipo_pelicula;

    public int getId_precio_entrada_fk() {
        return id_precio_entrada_fk;
    }

    public void setId_precio_entrada_fk(int id_precio_entrada_fk) {
        this.id_precio_entrada_fk = id_precio_entrada_fk;
    }

    public int getId_tipo_pelicula() {
        return id_tipo_pelicula;
    }

    public void setId_tipo_pelicula(int id_tipo_pelicula) {
        this.id_tipo_pelicula = id_tipo_pelicula;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getTipo_pelicula() {
        return tipo_pelicula;
    }

    public void setTipo_pelicula(String tipo_pelicula) {
        this.tipo_pelicula = tipo_pelicula;
    }
}
