/**
 * Created by Marvin on 15/03/2017.
 */
package com.example.marvin.navigationdrawer;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonCines;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link infoDesarrollador.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link infoDesarrollador#newInstance} factory method to
 * create an instance of this fragment.
 */
public class infoDesarrollador extends Fragment {
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_info_desarrollador, container, false);

        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public void PantPrincipal()
    {
        Fragment newFragment = new Principal();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }
}


