package com.example.marvin.navigationdrawer;

import android.app.Application;

/**
 * Created by Marvin on 22/03/2017.
 */
public  class  GlobalData extends  Application {
        private String _Menu="Normal";
        public String get_Menu() {
            return _Menu;
        }
        public void set_Menu(String _menu) {
            this._Menu = _menu;
        }

}
