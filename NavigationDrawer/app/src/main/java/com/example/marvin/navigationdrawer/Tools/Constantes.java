package com.example.marvin.navigationdrawer.Tools;

/**
 * Created by DELL on 01/11/2016.
 */
public class Constantes {
    /**
     * Transición Home -> Detalle
     */
    public static final int CODIGO_DETALLE = 100;

    /**
     * Transición Detalle -> Actualización
     */
    public static final int CODIGO_ACTUALIZACION = 101;
    /**
     * Puerto que utilizas para la conexión.
     * Dejalo en blanco si no has configurado esta carácteristica.
     */
    private static final String PUERTO_HOST = ":80";
    /**
     * Dirección IP de genymotion o AVD
     */
    private static final String IP = "http://www.oneway.com.sv";
    /**
     * URLs del Web Service
     */


    public static final String nuevo_cine = IP + PUERTO_HOST + "/CineMax/Cine/nuevo_cine.php";
    public static final String actualizar_cine = IP + PUERTO_HOST + "/CineMax/Cine/actualizar_cine.php";
    public static final String eliminar_cine = IP + PUERTO_HOST + "/CineMax/Cine/eliminar_cine.php";
    public static final String obtener_cines = IP + PUERTO_HOST + "/CineMax/Cine/obtener_cines.php";
    public static final String obtener_cines_filtrado = IP + PUERTO_HOST + "/CineMax/Cine/cine_filtrado.php";

    public static final String nueva_pelicula = IP + PUERTO_HOST + "/CineMax/Pelicula/nueva_pelicula.php";
    public static final String actualizar_pelicula = IP + PUERTO_HOST + "/CineMax/Pelicula/actualizar_pelicula.php";
    public static final String eliminar_pelicula = IP + PUERTO_HOST + "/CineMax/Pelicula/eliminar_pelicula.php";
    public static final String obtener_peliculas = IP + PUERTO_HOST + "/CineMax/Pelicula/obtener_peliculas.php";
    public static final String obtener_pelicula_filtrada = IP + PUERTO_HOST + "/CineMax/Pelicula/pelicula_filtrada.php";

    public static final String nueva_sala = IP + PUERTO_HOST + "/CineMax/Salas/nueva_sala.php";
    public static final String actualizar_sala = IP + PUERTO_HOST + "/CineMax/Salas/actualizar_sala.php";
    public static final String eliminar_sala = IP + PUERTO_HOST + "/CineMax/Salas/eliminar_sala.php";
    public static final String obtener_sala = IP + PUERTO_HOST + "/CineMax/Salas/obtener_salas.php";
    public static final String obtener_sala_filtrada = IP + PUERTO_HOST + "/CineMax/Salas/sala_filtrada.php";


    public static final String nuevo_usuario = IP + PUERTO_HOST + "/CineMax/Usuario/nuevo_usuario.php";
    public static final String actualizar_usuario = IP + PUERTO_HOST + "/CineMax/Usuario/actualizar_usuario.php";
    public static final String eliminar_usuario = IP + PUERTO_HOST + "/CineMax/Usuario/eliminar_usuario.php";
    public static final String obtener_usuarios = IP + PUERTO_HOST + "/CineMax/Usuario/obtener_usuarios.php";
    public static final String obtener_usuario_filtrada = IP + PUERTO_HOST + "/CineMax/Usuario/usuario_filtrado.php";

    public static final String nuevo_tipo = IP + PUERTO_HOST + "/CineMax/TipoPelicula/nuevo_tipo.php";
    public static final String actualizar_tipo = IP + PUERTO_HOST + "/CineMax/TipoPelicula/actualizar_tipo.php";
    public static final String eliminar_tipo = IP + PUERTO_HOST + "/CineMax/TipoPelicula/eliminar_tipo.php";
    public static final String obtener_tipos = IP + PUERTO_HOST + "/CineMax/TipoPelicula/obtener_tipos.php";
    public static final String obtener_tipo_filtrado = IP + PUERTO_HOST + "/CineMax/TipoPelicula/obtener_tipo_filtrado.php";




    /**
     * Clave para el valor extra que representa al identificador de una meta
     */
    public static final String EXTRA_ID = "IDEXTRA";


}
