/**
 * Created by Marvin on 15/03/2017.
 */
package com.example.marvin.navigationdrawer;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.webkit.WebView;

import com.example.marvin.navigationdrawer.MantoCines.admonCines;
import com.example.marvin.navigationdrawer.MantoCines.lCines;
import com.example.marvin.navigationdrawer.MantoPeliculas.lPeliculas;
import com.example.marvin.navigationdrawer.MantoPeliculas.rPeliculas;
import com.example.marvin.navigationdrawer.MantoSalas.admon_salas;
import com.example.marvin.navigationdrawer.MantoSalas.lSalas;
import com.example.marvin.navigationdrawer.MantoTipoPelicula.admonTipoPelicula;
import com.example.marvin.navigationdrawer.MantoTipoPelicula.lTipoPelicula;
import com.example.marvin.navigationdrawer.MantoUsuarios.admonUsuarios;
import com.example.marvin.navigationdrawer.MantoUsuarios.lUsuarios;
import com.example.marvin.navigationdrawer.cliente.mis_boletos;
import com.example.marvin.navigationdrawer.cliente.s_pelicula;
import com.example.marvin.navigationdrawer.cliente.ubicacion;



// hola
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    WebView myWebView;
    NavigationView navigationView;
    String TipoMenu = "Normal";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            Intent in = getIntent();
            Bundle b = in.getExtras();
            TipoMenu = b.getString("Valor");
        } catch (Exception e){
            //throw new IOException(e.toString());
        }

         myWebView = (WebView) this.findViewById(R.id.webView);
         myWebView.setVisibility(View.VISIBLE);
        if(TipoMenu.equals("Normal"))
        {
            hideItemAdmin();
        }else
        {
            hideItemUsuario();
        }

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Fragment f = null;
        f = new Principal();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, f).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if(TipoMenu.equals("Normal"))
        {
            hideItemAdmin();
        }else
        {
            hideItemUsuario();
        }
        int id = item.getItemId();
        myWebView.setVisibility(View.INVISIBLE);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_facebook) {
            Fragment f = null;
            f = new Principal();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, f).commit();
            myWebView.setVisibility(View.VISIBLE);
            myWebView.loadUrl("https://www.facebook.com/MawInvitaciones");
        }
        else if (id == R.id.action_inicioSesion) {
            Fragment f = null;
            f = new inicioSession();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, f).commit();
            item.setChecked(true);
            getSupportActionBar().setTitle(item.getTitle());
        }
        else if (id == R.id.action_Info) {
            Fragment f = null;
            f = new infoDesarrollador();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, f).commit();
            item.setChecked(true);
            getSupportActionBar().setTitle(item.getTitle());
        }
        else if (id == R.id.action_salir) {
            finish();
        }


        return super.onOptionsItemSelected(item);
    }


    private void hideItemAdmin()
    {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_programacionpeliculas).setVisible(false);
        nav_Menu.findItem(R.id.nav_icines).setVisible(false);
        nav_Menu.findItem(R.id.nav_isalas).setVisible(false);
        nav_Menu.findItem(R.id.nav_icines).setVisible(false);
        nav_Menu.findItem(R.id.nav_ipeliculas).setVisible(false);
        nav_Menu.findItem(R.id.nav_lcines).setVisible(true);
        nav_Menu.findItem(R.id.nav_lusuarios).setVisible(false);
        nav_Menu.findItem(R.id.nav_iusuarios).setVisible(false);
        nav_Menu.findItem(R.id.nav_lsalas).setVisible(false);
        nav_Menu.findItem(R.id.nav_lpeliculas).setVisible(true);
        nav_Menu.findItem(R.id.nav_rtop).setVisible(false);
        nav_Menu.findItem(R.id.nav_rventas).setVisible(false);
        nav_Menu.findItem(R.id.nav_itipospeliculas).setVisible(false);
        nav_Menu.findItem(R.id.nav_salirAdmin).setVisible(false);

        nav_Menu.findItem(R.id.nav_mis_boletos).setVisible(true);
        nav_Menu.findItem(R.id.nav_reserva).setVisible(true);
        nav_Menu.findItem(R.id.nav_ubicacion).setVisible(true);
        nav_Menu.findItem(R.id.nav_salir).setVisible(true);
    }

    private void hideItemUsuario()
    {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_mis_boletos).setVisible(false);
        nav_Menu.findItem(R.id.nav_reserva).setVisible(false);
        nav_Menu.findItem(R.id.nav_ubicacion).setVisible(false);
        nav_Menu.findItem(R.id.nav_salir).setVisible(false);



        nav_Menu.findItem(R.id.nav_programacionpeliculas).setVisible(true);
        nav_Menu.findItem(R.id.nav_icines).setVisible(true);
        nav_Menu.findItem(R.id.nav_isalas).setVisible(true);
        nav_Menu.findItem(R.id.nav_icines).setVisible(true);
        nav_Menu.findItem(R.id.nav_ipeliculas).setVisible(true);
        nav_Menu.findItem(R.id.nav_lcines).setVisible(true);
        nav_Menu.findItem(R.id.nav_lusuarios).setVisible(true);
        nav_Menu.findItem(R.id.nav_iusuarios).setVisible(true);
        nav_Menu.findItem(R.id.nav_lsalas).setVisible(true);
        nav_Menu.findItem(R.id.nav_lpeliculas).setVisible(true);
        nav_Menu.findItem(R.id.nav_rtop).setVisible(true);
        nav_Menu.findItem(R.id.nav_rventas).setVisible(true);
        nav_Menu.findItem(R.id.nav_itipospeliculas).setVisible(true);
        nav_Menu.findItem(R.id.nav_salirAdmin).setVisible(true);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        item.setChecked(false);
        Fragment f = null;
        myWebView.setVisibility(View.INVISIBLE);
        boolean fragmentSeleccionado = false;
        if (id == R.id.nav_programacionpeliculas) {
            f = new ProgramacionPeliculas();
            fragmentSeleccionado = true;
        }
        else if (id == R.id.nav_icines) {
            f = new admonCines();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_isalas) {
            f = new admon_salas();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_itipospeliculas) {
            f = new admonTipoPelicula();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_ipeliculas) {
            f = new rPeliculas();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_iusuarios) {
            f = new admonUsuarios();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_lcines) {
            f = new lCines();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_lsalas) {
            f = new lSalas();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_ltipospeliculas) {
            f = new lTipoPelicula();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_lpeliculas) {
            f = new lPeliculas();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_lusuarios) {
            f = new lUsuarios();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_rventas) {
            f = new reVentas();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_rtop) {
            f = new reTop();
            fragmentSeleccionado = true;
        }else if (id == R.id.nav_reserva) {
            f = new s_pelicula();
            fragmentSeleccionado = true;
        }
        else if (id == R.id.nav_mis_boletos) {
            f = new mis_boletos();
            fragmentSeleccionado = true;
        }
        else if (id == R.id.nav_ubicacion) {
            f = new ubicacion();
            fragmentSeleccionado = true;
        }
        else if (id == R.id.nav_salir) {
            finish();
        }
        else if (id == R.id.nav_salirAdmin) {
           hideItemAdmin();
            f = new Principal();
            fragmentSeleccionado = true;
        }

        if (fragmentSeleccionado) {
           // getSupportFragmentManager().beginTransaction().replace(R.id.content_main, f).commit();
            Fragment newFragment = new Fragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.content_main, f);
            transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commit();



            NavigationView mNavigationView = (NavigationView)findViewById(R.id.nav_view);
            uncheckAllMenuItems(mNavigationView);
/*
            int size = mNavigationView.getMenu().size();

            for (int i = 0; i < size; i++) {
                mNavigationView.getMenu().getItem(i).setChecked(false);
            }
*/
            item.setChecked(true);


            getSupportActionBar().setTitle(item.getTitle());

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void uncheckAllMenuItems(NavigationView navigationView) {
        final Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.hasSubMenu()) {
                SubMenu subMenu = item.getSubMenu();
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    subMenuItem.setChecked(false);
                }
            } else {
                item.setChecked(false);
            }
        }
    }
}

