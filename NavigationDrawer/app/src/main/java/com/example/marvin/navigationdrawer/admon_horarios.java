package com.example.marvin.navigationdrawer;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonCines;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link admon_horarios.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link admon_horarios#newInstance} factory method to
 * create an instance of this fragment.
 */
public class admon_horarios extends Fragment
{

    View v;

    ListView lvListarUsuario;
    EditText buscarUsuario;
    ArrayAdapter<String> adaptador;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_admon_horarios, container, false);
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
