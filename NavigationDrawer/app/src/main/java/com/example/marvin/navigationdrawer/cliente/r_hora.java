package com.example.marvin.navigationdrawer.cliente;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link r_hora.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class r_hora extends Fragment {

    private OnFragmentInteractionListener mListener;

    public r_hora() {
        // Required empty public constructor
    }

    View view;
    Button btnAtrasHora;
    Button btnSiguienteHora;
    Button btnCancelarHora;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_r_hora, container, false);

        btnAtrasHora = (Button)view.findViewById(R.id.btnAtrasHora);
        btnSiguienteHora = (Button)view.findViewById(R.id.btnSiguienteHora);
        btnCancelarHora = (Button)view.findViewById(R.id.btnCancelarHora);

        return view;

    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        btnCancelarHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new Principal();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });

        btnSiguienteHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new r_lugar();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });

        btnAtrasHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new r_datosPersonales();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
