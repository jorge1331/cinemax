package com.example.marvin.navigationdrawer.AdmonElement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marvin on 15/03/2017.
 */
public class AdmonSalas
{
    SQLiteDatabase db;
    SQLiteManager manager;

    private int idSala, idCineFk, numeroButacas;
    private String nombreSala;
    private int idCine;

    public String tableName = "salas";
    public String id_sala = "id_salas", nombre_salas = "nombre_salas", numero_butacas = "numero_butacas", id_cine_fk = "id_cine_fk";


    public AdmonSalas(Context context)
    {
        manager = new SQLiteManager(context, "ETPS3_AvanceCine", null, 1);
        db = manager.getWritableDatabase();
    }


    public int NuevoSala(AdmonSalas Sala)
    {
        int cod  = getIdCine();
        ConocerIdCine(cod);

        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put("nombre_salas", Sala.getNombreSala());
            Valores.put("numero_butacas", Sala.getNumeroButacas());
            Valores.put("id_cine_fk", Sala.getIdCineFk());
            this.manager.getWritableDatabase().insert(tableName, null, Valores);
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }

    public void ConocerIdCine(int id)
    {
        int codC;
        //Obtengo en un listado todos los cines en existencia que esten activos
        List<String> AllCines= GetAllCine(this);

        //En una nueva variable obtengo el nombre en la posicion recibido como parametro
        String nombreCine = AllCines.get(id);

        //Hago nueva consulta para saber cual es el id del cine con el nombre ya obtenido
        String[] campos = new String[] {"id_cine"};
        String[] args = new String[] {nombreCine};

        Cursor c = db.query("cines", campos, "nombre_cine=?", args, null, null, null);

        if (c.moveToFirst())
        {
            //Recorremos el cursor hasta que no haya más registros
            do {
                codC = c.getInt(0);
                setIdCineFk(codC);
            } while (c.moveToNext());

        }

    }


    public List<String> SelectSalas(AdmonSalas Sala) {
        String sql = "select * from salas where isActive = 1";
        List<String> list = new ArrayList<>();
        Cursor matriz = this.db.rawQuery(sql, null);
        while(matriz.moveToNext()) {
            list.add(matriz.getString(1).toString());
        }
        return list;
    }


    public List<String> SearchSala(AdmonSalas Sala) {
        String sql = "select * from salas where isActive = 1 and "+nombre_salas+" = '"+ Sala.getNombreSala()+"'";
        List<String> list = new ArrayList<>();
        Cursor matriz = this.db.rawQuery(sql, null);
        while(matriz.moveToNext()) {
            list.add(matriz.getString(1).toString());
        }
        return list;
    }


    public Cursor DatosAEditarSala(AdmonSalas Sala)
    {
        String[] campos = new String[] {id_sala, nombre_salas, numero_butacas, id_cine_fk};
        String[] args = new String[] {getNombreSala()};

        Cursor matriz = db.query(tableName, campos, "nombre_salas=?", args, null, null, null);

        return matriz;
    }


    public int EditarSala (AdmonSalas Sala)
    {
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put(nombre_salas, Sala.getNombreSala());
            Valores.put(numero_butacas, Sala.getNumeroButacas());
            Valores.put(id_cine_fk, Sala.getIdCineFk());
            this.manager.getWritableDatabase().update(tableName,Valores,"id_salas=?",new String[]{(String.valueOf(getIdSala()))});
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }



    public int EliminarSala (AdmonSalas Sala)
    {
        //En lugar de eliminar el registro de un cine, lo que se hace es actualizar el registro
        //en el campo isActive con numero diferente de 1, por estandarizacion 0.
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put("isActive", 0);

            this.manager.getWritableDatabase().update(tableName,Valores,"id_salas=?",new String[]{(String.valueOf(getIdSala()))});
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }



    public List<String> GetAllCine(AdmonSalas Sala)
    {
        String sql = "select * from cines where active = 1";
        List<String> list = new ArrayList<>();
        Cursor matriz = this.db.rawQuery(sql, null);
        while(matriz.moveToNext())
        {
            list.add(matriz.getString(1).toString());
        }
        return list;
    }

    public Cursor GetCine(AdmonSalas Sala)
    {
        String[] campos = new String[] {"nombre_cine"};
        String[] args = new String[] {String.valueOf(getIdCine())};

        Cursor matriz = db.query("cines", campos, "id_cine=?", args, null, null, null);

        return matriz;
    }



    public int getIdSala() { return idSala; }

    public void setIdSala(int idSala) { this.idSala = idSala; }

    public int getIdCineFk() { return idCineFk; }

    public void setIdCineFk(int idCineFk) { this.idCineFk = idCineFk; }

    public int getNumeroButacas() { return numeroButacas; }

    public void setNumeroButacas(int numeroButacas) { this.numeroButacas = numeroButacas; }

    public String getNombreSala() { return nombreSala; }

    public void setNombreSala(String nombreSala) { this.nombreSala = nombreSala; }

    public int getIdCine() { return idCine; }

    public void setIdCine(int idCine) { this.idCine = idCine; }
}
