package com.example.marvin.navigationdrawer.AdmonElement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by garcia on 17/04/2017.
 */

public class AdmonTipoPelicula
{

    SQLiteDatabase db;
    SQLiteManager manager;

    private int idPeliculaTipo;
    private String tipoPelicula, descripcionTipoPelicula;
    private double precioTipoPelicula;


    String tableName = "pelicula_tipo";
    String id_pelicula_tipo ="id_pelicula_tipo", tipo_pelicula = "tipo_pelicula", descripcion = "descripcion", precio_entrada = "precio_entrada";


    public AdmonTipoPelicula(Context context)
    {
        manager = new SQLiteManager(context, "ETPS3_AvanceCine", null, 1);
        db = manager.getWritableDatabase();
    }

    public int NuevoTipoPelicula(AdmonTipoPelicula TP )
    {
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put(tipo_pelicula, TP.getTipoPelicula());
            Valores.put(descripcion, TP.getDescripcionTipoPelicula());
            Valores.put(precio_entrada, TP.getPrecioTipoPelicula());
            this.manager.getWritableDatabase().insert(tableName, null, Valores);
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }


    public List<String> SelectTipoPelicula(AdmonTipoPelicula TP)
    {
        String sql = "select * from "+tableName+" where isActive = 1";
        List<String> list = new ArrayList<>();
        Cursor matriz = this.db.rawQuery(sql, null);
        while(matriz.moveToNext()) {
            list.add(matriz.getString(1).toString());
        }
        return list;
    }


    public List<String> SearchTipoPelicula(AdmonTipoPelicula TP)
    {
        String sql = "select * from "+tableName+" where isActive = 1 and "+tipo_pelicula+" = '"+ TP.getTipoPelicula()+"'";
        List<String> list = new ArrayList<>();
        Cursor matriz = this.db.rawQuery(sql, null);
        while(matriz.moveToNext()) {
            list.add(matriz.getString(1).toString());
        }
        return list;
    }


    public Cursor DatosAEditarTipoPelicula(AdmonTipoPelicula TP)
    {
        String[] campos = new String[] {id_pelicula_tipo, tipo_pelicula, descripcion, precio_entrada};
        String[] args = new String[] {getTipoPelicula()};

        Cursor matriz = db.query(tableName, campos, ""+tipo_pelicula+"=?", args, null, null, null);

        return matriz;
    }



    public int EditarTipoPelicula (AdmonTipoPelicula TP)
    {
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put(tipo_pelicula, TP.getTipoPelicula());
            Valores.put(descripcion, TP.getDescripcionTipoPelicula());
            Valores.put(precio_entrada, TP.getPrecioTipoPelicula());
            this.manager.getWritableDatabase().update(tableName,Valores,""+id_pelicula_tipo+"=?",new String[]{(String.valueOf(getIdPeliculaTipo()))});
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }



    public int EliminarTipoPelicula (AdmonTipoPelicula TP)
    {
        //En lugar de eliminar el registro de un tipo de pelicula, lo que se hace es actualizar el registro
        //en el campo isActive con numero diferente de 1, por estandarizacion 0.
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put("isActive", 0);

            this.manager.getWritableDatabase().update(tableName,Valores,""+id_pelicula_tipo+"=?",new String[]{(String.valueOf(getIdPeliculaTipo()))});
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }




    public int getIdPeliculaTipo() { return idPeliculaTipo; }

    public void setIdPeliculaTipo(int idPeliculaTipo) { this.idPeliculaTipo = idPeliculaTipo; }

    public String getTipoPelicula() { return tipoPelicula; }

    public void setTipoPelicula(String tipoPelicula) { this.tipoPelicula = tipoPelicula; }

    public String getDescripcionTipoPelicula() { return descripcionTipoPelicula; }

    public void setDescripcionTipoPelicula(String descripcionTipoPelicula)
    {
        this.descripcionTipoPelicula = descripcionTipoPelicula;
    }

    public double getPrecioTipoPelicula() {
        return precioTipoPelicula;
    }

    public void setPrecioTipoPelicula(double precioTipoPelicula) {
        this.precioTipoPelicula = precioTipoPelicula;
    }
}
