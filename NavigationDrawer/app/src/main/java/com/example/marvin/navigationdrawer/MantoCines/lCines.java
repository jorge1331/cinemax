/**
 * Created by Marvin on 15/03/2017.
 */
package com.example.marvin.navigationdrawer.MantoCines;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonCines;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;
import com.example.marvin.navigationdrawer.Web.BaseBolleyFragment;
import com.example.marvin.navigationdrawer.Web.BaseVolleyFragment;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.marvin.navigationdrawer.R.drawable.cine;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link lCines.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link lCines#newInstance} factory method to
 * create an instance of this fragment.
 */
public class lCines extends Fragment {
    View v;
    ListView lvCinesV;
    private Gson gson = new Gson();
    Cine[] cine;
    List<String> allCines;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_l_cines, container, false);
        lvCinesV = (ListView)v.findViewById(R.id.lvCinesL);
        CargaData();
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
    }

    public void CargaData()
    {
        SelectCines();
    }

    public void SelectCines() {
        makeRequest();
    }

    private void makeRequest() {

        String url = Constantes.obtener_cines;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                procesarRespuesta(response);
                onConnectionFinished();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                onConnectionFailed(volleyError.toString());
            }
        });
        addToQueue(request);

    }


    private void procesarRespuesta(JSONObject response) {
        try {
            // Obtener atributo "estado"
            String estado = response.getString("estado");
            switch (estado) {
                case "1": // EXITO
                    // Obtener el array "usuario"
                    JSONArray object = response.getJSONArray("cines");
                    // Asignamos el resultado del JSON a nuestro modelo Usuarios
                    cine = gson.fromJson(object.toString(), Cine[].class);

                    allCines = new ArrayList<String>();
                    for(int i=0; i<cine.length; i++)
                    {
                        allCines.add(cine[i].getNombre_cine());
                    }
                    ArrayAdapter<String> itemsAdapter =
                            new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,allCines);
                    lvCinesV.setAdapter(itemsAdapter);

                    break;
                case "2": // FALLIDO
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            getActivity(),
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {

        }

    }


    public void PantPrincipal()
    {
        Fragment newFragment = new rCines();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }

    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;




    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }
}
