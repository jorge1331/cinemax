/**
 * Created by Marvin on 15/03/2017.
 */
package com.example.marvin.navigationdrawer.MantoUsuarios;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonUsuarios;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.Models.Usuarios;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.example.marvin.navigationdrawer.R.drawable.cine;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link rUsuarios.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link rUsuarios#newInstance} factory method to
 * create an instance of this fragment.
 */
public class rUsuarios extends Fragment {
    View v;
    EditText nombreUsuario, apellidoUsuario, direccionUsuario, correoUsuario, usuarioUsuario, claveUsuario;
    EditText telefonoUsuario, duiUsuario;
    Spinner cargo_Usuario;
    Button BtnGuardar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_r_usuarios, container, false);
        LlenarSpCargo();
        BtnGuardar = (Button)v.findViewById(R.id.btnGuardarUsuario);
        return v;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GuardarUsuario();
            }
        });

        Button Btn = (Button)v.findViewById(R.id.btnPrincipalrusuario);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(1);
            }
        });

    }

    public void LlenarSpCargo()
    {
        cargo_Usuario= (Spinner) v.findViewById(R.id.spCargoUsuario);

        String[] cargos = getResources().getStringArray(R.array.CargoUsuariosArray);

        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1,cargos);

        cargo_Usuario.setAdapter(adapter);


    }

    public void GuardarUsuario()
    {
        nombreUsuario = (EditText) v.findViewById(R.id.edtNombreUsuario);
        apellidoUsuario = (EditText) v.findViewById(R.id.edtApellidoUsuario);
        direccionUsuario = (EditText) v.findViewById(R.id.edtDireccionUsuario);
        telefonoUsuario = (EditText) v.findViewById(R.id.edtTelefonoUsuario);
        correoUsuario = (EditText) v.findViewById(R.id.edtCorroUsuario);
        duiUsuario = (EditText) v.findViewById(R.id.edtDuiUsuario);
        usuarioUsuario = (EditText) v.findViewById(R.id.edtUsuarioUsuario);
        claveUsuario = (EditText) v.findViewById(R.id.edtPasswordUsuario);

        cargo_Usuario = (Spinner)v.findViewById(R.id.spCargoUsuario);

        if((TextUtils.isEmpty(nombreUsuario.getText())) || (TextUtils.isEmpty(apellidoUsuario.getText())) || (TextUtils.isEmpty(direccionUsuario.getText())))
        {
            Toast.makeText(getActivity(), "Rellene todos los campos", Toast.LENGTH_LONG).show();
        }
        else if((TextUtils.isEmpty(duiUsuario.getText())) || (TextUtils.isEmpty(telefonoUsuario.getText())) || (TextUtils.isEmpty(usuarioUsuario.getText())))
        {
            Toast.makeText(getActivity(), "Rellene todos los campos", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(claveUsuario.getText()))
        {
            Toast.makeText(getActivity(), "Rellene todos los campos", Toast.LENGTH_LONG).show();
        }
        else
        {
            Usuarios usu = new Usuarios();

            usu.setNombre_usuario(nombreUsuario.getText().toString());
            usu.setAppelido_usuario(apellidoUsuario.getText().toString());
            usu.setDireccion_usuario(direccionUsuario.getText().toString());
            usu.setTelefono_usuario(telefonoUsuario.getText().toString());
            usu.setCorreo_usuario(correoUsuario.getText().toString());
            usu.setDui_usuario(duiUsuario.getText().toString());
            usu.setUsuario_usuario(usuarioUsuario.getText().toString());
            usu.setClave_usuario(claveUsuario.getText().toString());

            String Resp = NuevoUsuario(usu);

            if(Resp == "correcto")

            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                PantPrincipal(1);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new admonUsuarios();
        if(FragementEle == 1) {
            newFragment = new admonUsuarios();
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }

    public String NuevoUsuario(Usuarios usuarios)
    {
        try
        {
            String url = "";
            url = Constantes.nuevo_usuario;


            //int lab = spnLaboratorios.getSelectedItemPosition();
            //int Objeto = spnObjeto.getSelectedItemPosition();

            HashMap<String, String> map = new HashMap<>();// MAPEO
            map.put("apellido_usuario", usuarios.getNombre_usuario());
            map.put("cargoUsuario_id", "1");
            map.put("clave_usuario", usuarios.getClave_usuario());
            map.put("correo_usuario", usuarios.getCorreo_usuario());
            map.put("direccion_usuario", usuarios.getDireccion_usuario());
            map.put("dui_usuario", usuarios.getDui_usuario());
            map.put("nombre_usuario", usuarios.getNombre_usuario());
            map.put("telefono_usuario", usuarios.getTelefono_usuario());
            map.put("usuario_usuario", usuarios.getUsuario_usuario());
            JSONObject jobject = new JSONObject(map);// Objeto Json

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jobject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    processRequestInsert(response);
                    onConnectionFinished();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    onConnectionFailed(volleyError.toString());
                }
            });
            addToQueue(request);
        }catch (Exception e)
        {
            return e.getMessage();
        }
        return "correcto";
    }



    private void processRequestInsert(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Registro agregado con exito",
                            Toast.LENGTH_LONG).show();
                    //  RedirectExit();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Ha ocurrido un error",
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    getActivity().setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    getActivity().finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

}