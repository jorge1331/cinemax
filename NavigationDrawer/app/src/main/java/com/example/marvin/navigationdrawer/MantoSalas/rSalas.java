/**
 * Created by Marvin on 15/03/2017.
 */
package com.example.marvin.navigationdrawer.MantoSalas;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonSalas;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.Models.Salas;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;
import com.example.marvin.navigationdrawer.Web.BaseVolleyFragment;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.marvin.navigationdrawer.R.drawable.cine;
import static com.example.marvin.navigationdrawer.R.id.lvListarCine;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link rSalas.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link rSalas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class rSalas extends Fragment
{
    
    View v;
    EditText nombreSala, numButacas;
    Spinner CineSala;
    int idCine;
    private Gson gson = new Gson();
    Cine[] cine;
    List<String> allCines;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) 
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_r_salas, container, false);
        CineSala = (Spinner) v.findViewById(R.id.spCineSala);
        LlenarSpinner();
        return v;
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        Button BtnGuardar = (Button)v.findViewById(R.id.btnGuardarSala);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GuardarSala();

            }
        });

        Button Btn = (Button)v.findViewById(R.id.btnAtrasSalaR);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(1);
            }
        });


        //Obtengo en valor selecionado del spinner para luego conocer que cine es
        //es valor es el que se pasa a la clase controlador
        CineSala.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l)
            {
                idCine = (position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {
                idCine = 0;
            }
        });


    }



    public void LlenarSpinner()
    {

        llenarCines();
    }

    private void llenarCines() {

        String url = Constantes.obtener_cines;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                procesarRespuesta(response);
                onConnectionFinished();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                onConnectionFailed(volleyError.toString());
            }
        });
        addToQueue(request);

    }


    private void procesarRespuesta(JSONObject response) {
        try {
            // Obtener atributo "estado"
            String estado = response.getString("estado");
            switch (estado) {
                case "1": // EXITO
                    // Obtener el array "usuario"
                    JSONArray object = response.getJSONArray("cines");
                    // Asignamos el resultado del JSON a nuestro modelo Usuarios
                    cine = gson.fromJson(object.toString(), Cine[].class);

                    allCines = new ArrayList<String>();
                    for(int i=0; i<cine.length; i++)
                    {
                        allCines.add(cine[i].getNombre_cine());
                    }

                    CineSala = (Spinner) v.findViewById(R.id.spCineSala);

                    ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, allCines);

                    adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    CineSala.setAdapter(adaptador);

                    break;
                case "2": // FALLIDO
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            getActivity(),
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {

        }

    }



    public void GuardarSala()
    {
        nombreSala = (EditText) v.findViewById(R.id.edtNombreSala);
        numButacas = (EditText) v.findViewById(R.id.edtNumeroButacas);
        
        CineSala = (Spinner) v.findViewById(R.id.spCineSala);

        if(TextUtils.isEmpty(nombreSala.getText()))
        {
            nombreSala.setFocusable(true);
            nombreSala.setError("Ingrese el nombre de la Sala");
        }
        else if(TextUtils.isEmpty(numButacas.getText()))
        {
            numButacas.setFocusable(true);
            numButacas.setError("Ingrese el numero de Butacas de la Sala");
        }
        else
        {
            Salas Sal = new Salas();

            //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
            Sal.setNombre_salas(nombreSala.getText().toString());
            Sal.setNumero_butacas(Integer.parseInt(numButacas.getText().toString()));
            Sal.setId_cine_fk(idCine);

            String Resp = NuevoSala(Sal);


            if(Resp == "correcto")
            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                PantPrincipal(1);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new admon_salas();
        if(FragementEle == 1) {
            newFragment = new admon_salas();
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }

    public String NuevoSala(Salas sala)
    {
        try
        {
            String url = "";
            url = Constantes.nueva_sala;


            //int lab = spnLaboratorios.getSelectedItemPosition();
            //int Objeto = spnObjeto.getSelectedItemPosition();


            HashMap<String, String> map = new HashMap<>();// MAPEO
            map.put("id_cine_fk", String.valueOf(sala.getId_cine_fk()));
            map.put("isActive", "1");
            map.put("nombre_salas", sala.getNombre_salas());
            map.put("numero_butacas",  String.valueOf(sala.getNumero_butacas()));
            JSONObject jobject = new JSONObject(map);// Objeto Json

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jobject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    processRequestInsert(response);
                    onConnectionFinished();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    onConnectionFailed(volleyError.toString());
                }
            });
            addToQueue(request);
        }catch (Exception e)
        {
            return e.getMessage();
        }
        return "correcto";
    }



    private void processRequestInsert(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Registro agregado con exito",
                            Toast.LENGTH_LONG).show();
                    //  RedirectExit();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Ha ocurrido un error",
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    getActivity().setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    getActivity().finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

}