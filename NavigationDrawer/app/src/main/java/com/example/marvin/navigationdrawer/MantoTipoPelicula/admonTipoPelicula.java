package com.example.marvin.navigationdrawer.MantoTipoPelicula;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonTipoPelicula;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link admonTipoPelicula.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link admonTipoPelicula#newInstance} factory method to
 * create an instance of this fragment.
 */
public class admonTipoPelicula extends Fragment
{
    View v;
    ListView lvListarTP;
    EditText buscarTP;
    ArrayAdapter<String> adaptador;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_admon_tipo_pelicula, container, false);
        lvListarTP = (ListView)v.findViewById(R.id.lvListarTP);
        CargaData();
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageButton BtnBuscar = (ImageButton) v.findViewById(R.id.btnBuscarTP);
        BtnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BuscarTipoPelicula();
            }
        });

        Button BtnAgregar = (Button)v.findViewById(R.id.btnAgregarTP);
        BtnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(2);
            }
        });

        final Button BtnMostrarTodo = (Button)v.findViewById(R.id.btnMostrarTodosTP);
        BtnMostrarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CargaData();
                BtnMostrarTodo.setVisibility(View.INVISIBLE);
            }
        });

        lvListarTP.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                //Consigo el nombre del item de la lista selecionada
                String tt = lvListarTP.getItemAtPosition(position).toString();

                //mMando a inicializar el nuevo fragment
                Fragment newFragment = new EditElimTipoPelicula();


                //Envio el nombre conseguido del usario seleccionado en la lista
                Bundle args = new Bundle();
                args.putString("tipoPelicula", tt);
                newFragment.setArguments(args);

                //Cambio de fragment
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();

            }
        });

    }

    public void BuscarTipoPelicula()
    {
        //Capturo lo escrito en el edittext buscar
        buscarTP = (EditText) v.findViewById(R.id.edtBuscarTP);

        //Evaluo si lo ingresado por el usuario es texto en blanco
        if(TextUtils.isEmpty(buscarTP.getText())) {}
        else
        {
            //Creo Objeto de la clase que se comunica con la bd
            AdmonTipoPelicula Tp = new AdmonTipoPelicula(getActivity());

            //Asigno lo capturado a la variable nombre de la clase que se comunica con la bd
            //por medio del set que luego en el metodo interno de la clase referenciada
            //se usara para la consulta
            Tp.setTipoPelicula(buscarTP.getText().toString());

            //Obtengo el resultado de la consulta por nombre
            List<String> allTp= Tp.SearchTipoPelicula(Tp);

            //Evaluo si el resultado esta vacio, lo que significaria que no exite registro
            //con ese nombre
            if (allTp.isEmpty())
            {
                Toast.makeText(getActivity(),"Sin Coincidencias",Toast.LENGTH_SHORT).show();
                CargaData();
            }else
            {
                //Limpio el adaptador del listview para volverlo a llenar con el resultado
                //de la busqueda y luego vuelvo a llenar el adaptador y se lo mando al listview
                adaptador.clear();
                adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allTp);
                lvListarTP.setAdapter(adaptador);

                Button todos = (Button) v.findViewById(R.id.btnMostrarTodosTP);
                todos.setVisibility(View.VISIBLE);

            }


        }
    }

    public void CargaData()
    {
        AdmonTipoPelicula Tp = new AdmonTipoPelicula(getActivity());
        List<String> allTp= Tp.SelectTipoPelicula(Tp);

        adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allTp);

        lvListarTP.setAdapter(adaptador);
    }

    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new Principal();
        if(FragementEle == 1)
        {
            newFragment = new Principal();
        }else if(FragementEle == 2)
        {
            newFragment = new rTipoPelicula();
        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }



}
