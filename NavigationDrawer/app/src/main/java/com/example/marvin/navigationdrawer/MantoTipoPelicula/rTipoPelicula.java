package com.example.marvin.navigationdrawer.MantoTipoPelicula;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonTipoPelicula;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.Models.TipoPeliculas;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.example.marvin.navigationdrawer.R.drawable.cine;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link rTipoPelicula.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link rTipoPelicula#newInstance} factory method to
 * create an instance of this fragment.
 */
public class rTipoPelicula extends Fragment
{

    EditText tipoPelicula, descripcionPelicula, precioEntradaTP;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_r_tipo_pelicula, container, false);

        return v;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button BtnGuardar = (Button)v.findViewById(R.id.btnGuardarTP);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GuardarTipoPelicula();

            }
        });

        Button Btn = (Button)v.findViewById(R.id.btnAtrasTPr);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(1);
            }
        });
    }


    public void GuardarTipoPelicula()
    {
        tipoPelicula = (EditText)v.findViewById(R.id.edtNombreTP);
        descripcionPelicula = (EditText)v.findViewById(R.id.edtDescripcionTP);
        precioEntradaTP = (EditText)v.findViewById(R.id.edtPrecioEntradaTP);

        if(TextUtils.isEmpty(tipoPelicula.getText()))
        {
            tipoPelicula.setFocusable(true);
            tipoPelicula.setError("Ingrese el nombre del Tipo de Pelicula");
        }
        else if(TextUtils.isEmpty(descripcionPelicula.getText()))
        {
            descripcionPelicula.setFocusable(true);
            descripcionPelicula.setError("Ingrese una breve descripcion");
        }
        else if(TextUtils.isEmpty(precioEntradaTP.getText()))
        {
            precioEntradaTP.setFocusable(true);
            precioEntradaTP.setError("Ingrese el precio de la entrada para este tipo de pelicula");
        }
        else
        {
            TipoPeliculas Tp = new TipoPeliculas();

            //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
            Tp.setTipo_pelicula(tipoPelicula.getText().toString());
            //Tp(descripcionPelicula.getText().toString());
            //Tp.setPrecioTipoPelicula(Double.parseDouble(precioEntradaTP.getText().toString()));

            String Resp = NuevoTipo(Tp);


            if(Resp == "correcto")
            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                PantPrincipal(1);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
            }
        }
    }


    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new admonTipoPelicula();
        if(FragementEle == 1) {
            newFragment = new admonTipoPelicula();
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }

    public String NuevoTipo(TipoPeliculas tipo)
    {
        try
        {
            String url = "";
            url = Constantes.nuevo_tipo;

            //int lab = spnLaboratorios.getSelectedItemPosition();
            //int Objeto = spnObjeto.getSelectedItemPosition();


            HashMap<String, String> map = new HashMap<>();// MAPEO
            map.put("tipo_pelicula", tipo.getTipo_pelicula());
            map.put("isActive", "1");
            map.put("id_precio_entrada_fk", "1");
            JSONObject jobject = new JSONObject(map);// Objeto Json

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jobject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    processRequestInsert(response);
                    onConnectionFinished();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    onConnectionFailed(volleyError.toString());
                }
            });
            addToQueue(request);
        }catch (Exception e)
        {
            return e.getMessage();
        }
        return "correcto";
    }



    private void processRequestInsert(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Registro agregado con exito",
                            Toast.LENGTH_LONG).show();
                    //  RedirectExit();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Ha ocurrido un error",
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    getActivity().setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    getActivity().finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }


}
