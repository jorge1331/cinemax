package com.example.marvin.navigationdrawer.MantoPeliculas;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonPeliculas;
import com.example.marvin.navigationdrawer.MantoUsuarios.infoUsuario;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link admonPeliculas.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link admonPeliculas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class admonPeliculas extends Fragment
{

    View v;
    ListView lvListarPeliculas;
    EditText buscarPelicula;
    ArrayAdapter<String> adaptador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_admon_peliculas, container, false);
        lvListarPeliculas = (ListView)v.findViewById(R.id.lvListarPeliculas);
        CargaData();
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        ImageButton BtnBuscar = (ImageButton)v.findViewById(R.id.btnBuscarPelicula);
        BtnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BuscarPelicula();
            }
        });

        Button BtnAgregar = (Button)v.findViewById(R.id.btnAgregarPelicula);
        BtnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(2);
            }
        });


        final Button BtnMostrarTodo = (Button)v.findViewById(R.id.btnMostrarTodosPeliculas);
        BtnMostrarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CargaData();
                BtnMostrarTodo.setVisibility(View.INVISIBLE);
            }
        });


        lvListarPeliculas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                //Consigo el nombre del item de la lista selecionada
                String nombrePelicula = lvListarPeliculas.getItemAtPosition(position).toString();

                //mMando a inicializar el nuevo fragment
                Fragment newFragment = new EditElimPeliculas();


                //Envio el nombre conseguido del usario seleccionado en la lista
                Bundle args = new Bundle();
                args.putString("NombrePelicula", nombrePelicula);
                newFragment.setArguments(args);

                //Cambio de fragment
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();

            }
        });

    }


    public void BuscarPelicula()
    {
        //Capturo lo escrito en el edittext buscar
        buscarPelicula = (EditText) v.findViewById(R.id.edtBuscarPelicula);

        //Evaluo si lo ingresado por el usuario es texto en blanco
        if(TextUtils.isEmpty(buscarPelicula.getText())) {}
        else
        {
            //Creo Objeto de la clase que se comunica con la bd
            AdmonPeliculas Pel = new AdmonPeliculas(getActivity());

            //Asigno lo capturado a la variable nombre de la clase que se comunica con la bd
            //por medio del set que luego en el metodo interno de la clase referenciada
            //se usara para la consulta
            Pel.setNombrePelicula(buscarPelicula.getText().toString());

            //Obtengo el resultado de la consulta por nombre
            List<String> allPeliculas= Pel.SearchPeliculas(Pel);

            //Evaluo si el resultado esta vacio, lo que significaria que no exite registro
            //con ese nombre
            if (allPeliculas.isEmpty())
            {
                Toast.makeText(getActivity(),"Sin Coincidencias",Toast.LENGTH_SHORT).show();
                CargaData();
            }else
            {
                //Limpio el adaptador del listview para volverlo a llenar con el resultado
                //de la busqueda y luego vuelvo a llenar el adaptador y se lo mando al listview
                adaptador.clear();
                adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allPeliculas);
                lvListarPeliculas.setAdapter(adaptador);

                Button todos = (Button) v.findViewById(R.id.btnMostrarTodosUsuario);
                todos.setVisibility(View.VISIBLE);

            }
        }
    }


    public void CargaData()
    {
        AdmonPeliculas Pel = new AdmonPeliculas(getActivity());
        List<String> allPeliculas = Pel.SelectPeliculas(Pel);

        adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allPeliculas);

        lvListarPeliculas = (ListView)v.findViewById(R.id.lvListarUsuarios);
        lvListarPeliculas.setAdapter(adaptador);
    }

    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new Principal();
        if(FragementEle == 1)
        {
            newFragment = new infoUsuario();
        }else if(FragementEle == 2)
        {
            newFragment = new rPeliculas();
        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }

    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

}
