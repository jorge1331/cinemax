package com.example.marvin.navigationdrawer.AdmonElement;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Marvin on 15/03/2017.
 */
public class SQLiteManager extends SQLiteOpenHelper
{
    public SQLiteManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Creacion de Tabla Cines
        String SQL = "create table cines(" +
                                            "id_cine integer primary key autoincrement, " +
                                            "nombre_cine text not null, " +
                                            "direccion varchar(150) not null," +
                                            "numeroSalas int(11)," +
                                            "latitud int," +
                                            "longitud int," +
                                            "active int not null default 1)";
        db.execSQL(SQL);

        //Creacion de Tabla salas
        SQL = "create table salas" +
                "(" +
                "  id_salas integer primary key autoincrement," +
                "  nombre_salas varchar(50)," +
                "  numero_butacas integer," +
                "  isActive integer(1) default 1," +
                "  id_cine_fk integer" +
                ")";
        db.execSQL(SQL);


        //Creacion de Tabla Peliculas
        SQL = "create table peliculas(" +
                "id_pelicula integer primary key autoincrement," +
                "  nombre_pelicula varchar(150)," +
                "  sinopsis_pelicula varchar(1000)," +
                "  director_pelicula varchar(150)," +
                "  escritor_pelicula varchar(150)," +
                "  productora_pelicula varchar(150)," +
                "  año_pelicula integer," +
                "  genero_pelicula varchar(50)," +
                "  clasificacion_pelicula varchar(10)," +
                "  isActive integer(1) default 1," +
                "  id_pelicula_tipo_fk integer)";
        db.execSQL(SQL);
        


        //Creacion de Tabla Usuarios
        SQL = "create table usuario(" +
                " id_usuario integer primary key autoincrement," +
                " nombre_usuario varchar(150)," +
                " apellido_usuario varchar(150)," +
                " edad_usuario varchar(150)," +
                " fecha_nac_usuario date," +
                " dui_usuario int(10)," +
                " direccion_usuario varchar(150)," +
                " telefono_usuario integer(8)," +
                " celular_usuario integer(8)," +
                " correo_usuario varchar(150)," +
                " usuario_usuario varchar(25)," +
                " clave_usario varchar(25)," +
                " cargoUsuario_id int not null, " +
                " isActive int not null default 1" +
                ")";
        db.execSQL(SQL);

        //Creacion de tabla Cargo Usuarios
        SQL = "create table cargousuario" +
                "(cargoUsuario_id int primary key not null autoincrement," +
                "nombre varchar(50) not null," +
                "descripcion varchar(150)," +
                "isActive int(11) default 1)";

        //Creacion de Tabla Tipo de peliculas
        SQL = "create table pelicula_tipo" +
                "(" +
                " id_pelicula_tipo integer primary key autoincrement," +
                " tipo_pelicula varchar(25) not null," +
                " descripcion varchar(150)," +
                " isActive integer default 1," +
                " precio_entrada double not null" +
                ");";
        db.execSQL(SQL);

        
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

