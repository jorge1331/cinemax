package com.example.marvin.navigationdrawer.AdmonElement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marvin on 15/03/2017.
 */
public class AdmonUsuarios {
    SQLiteDatabase db;
    SQLiteManager manager;

    private String nombreUsuario, apellidoUsuario, direccionUsuario, correoUsuario, usuarioUsuario, claveUsuario;
    private int idUsuario, telefonoUsuario, duiUsuario, cargo_Usuario;

    public String tableName = "usuario";
    public String id_usuario = "id_usuario", nombre_usuario = "nombre_usuario", apellido_usuario = "apellido_usuario";
    public String direccion_usuario = "direccion_usuario", telefono_usuario = "telefono_usuario";
    public String correo_usuario = "correo_usuario", dui_usuario = "dui_usuario", cargoUsuario = "cargoUsuario_id";
    public String usuario_usuario = "usuario_usuario", clave_usuario = "clave_usario";



    public AdmonUsuarios(Context context)
    {
        manager = new SQLiteManager(context, "ETPS3_AvanceCine", null, 1);
        db = manager.getWritableDatabase();
    }



    public int NuevoUsuario(AdmonUsuarios Usu)
    {
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put("nombre_usuario", Usu.getNombreUsuario());
            Valores.put("apellido_usuario", Usu.getApellidoUsuario());
            Valores.put("direccion_usuario", Usu.getDireccionUsuario());
            Valores.put("telefono_usuario", Usu.getTelefonoUsuario());
            Valores.put("correo_usuario", Usu.getCorreoUsuario());
            Valores.put("dui_usuario", Usu.getDuiUsuario());
            Valores.put("cargoUsuario_id", Usu.getCargo_Usuario());
            Valores.put("usuario_usuario", Usu.getUsuarioUsuario());
            Valores.put("clave_usario", Usu.getClaveUsuario());
            this.manager.getWritableDatabase().insert(tableName, null, Valores);
        }catch (Exception e)
        {
            return 0;
        }


        return 1;
    }


    public List<String> SearchUsuario(AdmonUsuarios Usu) {
        String sql = "select * from "+tableName+" where isActive = 1 and "+nombre_usuario+" = '"+ Usu.getNombreUsuario()+"'";
        List<String> list = new ArrayList<>();
        Cursor matriz = this.db.rawQuery(sql, null);
        while(matriz.moveToNext()) {
            list.add(matriz.getString(1).toString());
        }
        return list;
    }

        public List<String> SelectUsuarios(AdmonUsuarios Usu)
        {
            String sql = "select * from "+tableName+" where isActive = 1";
            List<String> list = new ArrayList<>();
            Cursor matriz = this.db.rawQuery(sql, null);
            while(matriz.moveToNext()) {
                list.add(matriz.getString(1).toString());
            }
            return list;
        }



    public Cursor DatosAEditarUsuario(AdmonUsuarios Usu)
    {
        String[] campos = new String[] {id_usuario, nombre_usuario, apellido_usuario, direccion_usuario, dui_usuario, telefono_usuario, correo_usuario,usuario_usuario, clave_usuario};
        String[] args = new String[] {getNombreUsuario()};

        Cursor matriz = db.query(tableName, campos, ""+nombre_usuario+"=?", args, null, null, null);

        return matriz;
    }

    public int EditarUsuarios (AdmonUsuarios Usu)
    {
        try
        {
            ContentValues Valores = new ContentValues();
                Valores.put("nombre_usuario", Usu.getNombreUsuario());
                Valores.put("apellido_usuario", Usu.getApellidoUsuario());
                Valores.put("direccion_usuario", Usu.getDireccionUsuario());
                Valores.put("telefono_usuario", Usu.getTelefonoUsuario());
                Valores.put("correo_usuario", Usu.getCorreoUsuario());
                Valores.put("dui_usuario", Usu.getDuiUsuario());
                Valores.put("cargoUsuario_id", Usu.getCargo_Usuario());
                Valores.put("usuario_usuario", Usu.getUsuarioUsuario());
                Valores.put("clave_usario", Usu.getClaveUsuario());
            this.manager.getWritableDatabase().update(tableName,Valores,""+id_usuario+"=?",new String[]{(String.valueOf(getIdUsuario()))});
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }


    public int EliminarUsuario (AdmonUsuarios Usu)
    {
        //En lugar de eliminar el registro de un cine, lo que se hace es actualizar el registro
        //en el campo isActive con numero diferente de 1, por estandarizacion 0.
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put("isActive", 0);

            this.manager.getWritableDatabase().update(tableName,Valores,""+id_usuario+"=?",new String[]{(String.valueOf(getIdUsuario()))});
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }



    //Getter and Setter de cada variable comunicadora entre la vista y bd


    public String getNombreUsuario() { return nombreUsuario; }

    public void setNombreUsuario(String nombreUsuario) { this.nombreUsuario = nombreUsuario; }

    public String getApellidoUsuario() { return apellidoUsuario; }

    public void setApellidoUsuario(String apellidoUsuario) { this.apellidoUsuario = apellidoUsuario; }

    public String getDireccionUsuario() { return direccionUsuario; }

    public void setDireccionUsuario(String direccionUsuario) { this.direccionUsuario = direccionUsuario; }

    public String getCorreoUsuario() { return correoUsuario; }

    public void setCorreoUsuario(String correoUsuario) { this.correoUsuario = correoUsuario; }

    public String getUsuarioUsuario() { return usuarioUsuario; }

    public void setUsuarioUsuario(String usuarioUsuario) { this.usuarioUsuario = usuarioUsuario; }

    public String getClaveUsuario() { return claveUsuario; }

    public void setClaveUsuario(String claveUsuario) { this.claveUsuario = claveUsuario; }

    public int getIdUsuario() { return idUsuario; }

    public void setIdUsuario(int idUsuario) { this.idUsuario = idUsuario; }

    public int getTelefonoUsuario() { return telefonoUsuario; }

    public void setTelefonoUsuario(int telefonoUsuario) { this.telefonoUsuario = telefonoUsuario; }

    public int getDuiUsuario() { return duiUsuario; }

    public void setDuiUsuario(int duiUsuario) { this.duiUsuario = duiUsuario; }

    public int getCargo_Usuario() { return cargo_Usuario; }

    public void setCargo_Usuario(int cargo_Usuario) { this.cargo_Usuario = cargo_Usuario; }
}
