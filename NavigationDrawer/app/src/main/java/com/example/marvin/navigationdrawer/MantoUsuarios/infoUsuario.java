package com.example.marvin.navigationdrawer.MantoUsuarios;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.marvin.navigationdrawer.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link infoUsuario.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link infoUsuario#newInstance} factory method to
 * create an instance of this fragment.
 */
public class infoUsuario extends Fragment {
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_info_usuario, container, false);
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button Btn = (Button)v.findViewById(R.id.btnAtrasInfoUsuario);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal();
            }
        });
    }

    public void PantPrincipal()
    {
        Fragment newFragment = new admonUsuarios();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }
}
