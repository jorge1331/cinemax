package com.example.marvin.navigationdrawer.MantoCines;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonCines;
import com.example.marvin.navigationdrawer.MantoUsuarios.infoUsuario;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.marvin.navigationdrawer.R.drawable.cine;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link admonCines.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link admonCines#newInstance} factory method to
 * create an instance of this fragment.
 */
public class admonCines extends Fragment

{
    View v;
    ListView lvListarCine;
    EditText buscarCine;
    ArrayAdapter<String> adaptador;
    private Gson gson = new Gson();
    Cine[] cine;
    List<String> allCines;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_admon_cines, container, false);
        lvListarCine = (ListView)v.findViewById(R.id.lvListarCine);
        CargaData();
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageButton BtnBuscar = (ImageButton) v.findViewById(R.id.btnBuscarCine);
        BtnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BuscarCine();
            }
        });

        Button BtnAgregar = (Button)v.findViewById(R.id.btnAgregarCine);
        BtnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(2);
            }
        });

        final Button BtnMostrarTodo = (Button)v.findViewById(R.id.btnMostrarTodosCine);
        BtnMostrarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CargaData();
                BtnMostrarTodo.setVisibility(View.INVISIBLE);
            }
        });

            lvListarCine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                //Consigo el nombre del item de la lista selecionada
                String nombreCine = lvListarCine.getItemAtPosition(position).toString();

                //mMando a inicializar el nuevo fragment
                Fragment newFragment = new EditElimCine();


                //Envio el nombre conseguido del usario seleccionado en la lista
                Bundle args = new Bundle();
                args.putString("NombreCine", nombreCine);
                newFragment.setArguments(args);

                //Cambio de fragment
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.content_main, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();

            }
        });

    }

    public void BuscarCine()
    {
        //Capturo lo escrito en el edittext buscar
        buscarCine = (EditText) v.findViewById(R.id.edtBuscarCine);

        //Evaluo si lo ingresado por el usuario es texto en blanco
        if(TextUtils.isEmpty(buscarCine.getText())) {}
        else
        {
            //Creo Objeto de la clase que se comunica con la bd
            AdmonCines Cin = new AdmonCines();

            //Asigno lo capturado a la variable nombre de la clase que se comunica con la bd
            //por medio del set que luego en el metodo interno de la clase referenciada
            //se usara para la consulta
            Cin.setNombreCine(buscarCine.getText().toString());

            //Obtengo el resultado de la consulta por nombre
            List<String> allCines= Cin.SearchCine(Cin);

            //Evaluo si el resultado esta vacio, lo que significaria que no exite registro
            //con ese nombre
            if (allCines.isEmpty())
            {
                Toast.makeText(getActivity(),"Sin Coincidencias",Toast.LENGTH_SHORT).show();
                CargaData();
            }else
            {
                //Limpio el adaptador del listview para volverlo a llenar con el resultado
                //de la busqueda y luego vuelvo a llenar el adaptador y se lo mando al listview
                adaptador.clear();
                adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allCines);
                lvListarCine.setAdapter(adaptador);

                Button todos = (Button) v.findViewById(R.id.btnMostrarTodosCine);
                todos.setVisibility(View.VISIBLE);

            }


        }
    }

    private void CargaData() {

        String url = Constantes.obtener_cines;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                procesarRespuesta(response);
                onConnectionFinished();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                onConnectionFailed(volleyError.toString());
            }
        });
        addToQueue(request);

    }


    private void procesarRespuesta(JSONObject response) {
        try {
            // Obtener atributo "estado"
            String estado = response.getString("estado");
            switch (estado) {
                case "1": // EXITO
                    // Obtener el array "usuario"
                    JSONArray object = response.getJSONArray("cines");
                    // Asignamos el resultado del JSON a nuestro modelo Usuarios
                    cine = gson.fromJson(object.toString(), Cine[].class);

                    allCines = new ArrayList<String>();
                    for(int i=0; i<cine.length; i++)
                    {
                        allCines.add(cine[i].getNombre_cine());
                    }
                    ArrayAdapter<String> itemsAdapter =
                            new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,allCines);
                    lvListarCine.setAdapter(itemsAdapter);

                    break;
                case "2": // FALLIDO
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            getActivity(),
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {

        }

    }

    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new Principal();
        if(FragementEle == 1)
        {
            newFragment = new infoUsuario();
        }else if(FragementEle == 2)
        {
            newFragment = new rCines();
        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }


    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }
}
