/**
 * Created by Marvin on 15/03/2017.
 */
package com.example.marvin.navigationdrawer.MantoPeliculas;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonPeliculas;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.Models.Pelicula;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.example.marvin.navigationdrawer.R.drawable.cine;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link rPeliculas.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link rPeliculas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class rPeliculas extends Fragment {
    EditText edtNombre;
    EditText edtCategoria;
    EditText edtTipo;
    View v;
    Button BtnGuardar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_r_peliculas, container, false);
        BtnGuardar = (Button)v.findViewById(R.id.btnGuardarPel);
        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GuardarCine();
            }
        });

        Button Btn = (Button)v.findViewById(R.id.btnPrincipalrpelicula);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal();
            }
        });
    }


    public void GuardarCine(){
        edtNombre = (EditText)v.findViewById(R.id.edtNombrePelicula);
        edtCategoria= (EditText)v.findViewById(R.id.edtCategoriaPel);
        edtTipo = (EditText)v.findViewById(R.id.edtTipoPel);
        if(TextUtils.isEmpty(edtNombre.getText()))
        {
            Toast.makeText(getActivity(), "Escriba el nombre de la Pelicula", Toast.LENGTH_LONG).show();
            edtNombre.setFocusable(true);
        }
        else if(TextUtils.isEmpty(edtCategoria.getText()))
        {
            Toast.makeText(getActivity(), "Escriba la categoria de la Pelicula", Toast.LENGTH_LONG).show();
            edtCategoria.setFocusable(true);
        }
        else if(TextUtils.isEmpty(edtTipo.getText()))
        {
            Toast.makeText(getActivity(), "Escriba el Tipo de la Pelicula", Toast.LENGTH_LONG).show();
            edtTipo.setFocusable(true);
        }
        else
        {
            Pelicula Pel = new Pelicula();
            AdmonPeliculas admP = new AdmonPeliculas(getActivity());

            Pel.setNombre_pelicula(edtNombre.getText().toString());
            Pel.setClasificacion_pelicula(edtCategoria.getText().toString());
            String Resp = NuevaPelicula(Pel);

            if(Resp == "correcto")
            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                edtNombre.setText("");
                edtCategoria.setText("");
                edtTipo.setText("");
            }
            else
            {
                Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void PantPrincipal()
    {
        Fragment newFragment = new Principal();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }

    public String NuevaPelicula(Pelicula pelicula)
    {
        try
        {
            String url = "";
            
            url = Constantes.nueva_pelicula;


            //int lab = spnLaboratorios.getSelectedItemPosition();
            //int Objeto = spnObjeto.getSelectedItemPosition();


            HashMap<String, String> map = new HashMap<>();// MAPEO
            map.put("nombre_pelicula", pelicula.getNombre_pelicula());
            map.put("clasificacion_pelicula", pelicula.getClasificacion_pelicula());
            map.put("isActive", "1");
            map.put("id_pelicula_tipo_fk", "1");
            JSONObject jobject = new JSONObject(map);// Objeto Json

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jobject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    processRequestInsert(response);
                    onConnectionFinished();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    onConnectionFailed(volleyError.toString());
                }
            });
            addToQueue(request);
        }catch (Exception e)
        {
            return e.getMessage();
        }
        return "correcto";
    }



    private void processRequestInsert(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Registro agregado con exito",
                            Toast.LENGTH_LONG).show();
                    //  RedirectExit();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Ha ocurrido un error",
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    getActivity().setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    getActivity().finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

}
