package com.example.marvin.navigationdrawer.AdmonElement;

import android.app.Activity;
import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;
import com.example.marvin.navigationdrawer.Web.BaseVolleyFragment;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Marvin on 15/03/2017.
 */
public class AdmonCines extends BaseVolleyFragment
{

    SQLiteDatabase db;
    SQLiteManager manager;

    private String NombreCine;
    private String DireccionCine;
    private int longitud;
    private int latitud;
    private int cine_id;

    public String tableName = "cines";
    public String nombreCine = "nombre_cine", direccionCine = "direccion", numSal = "numeroSalas", latitudCine = "latitud", longitudCine = "longitud", id = "id_cine";

    private ListView lisCines;


    public List<String> SelectCinesRepor(AdmonCines Cine) {
        String sql = "select * from cines where active = 1";
        List<String> list = new ArrayList<>();
        return list;
    }


    public List<String> SearchCine(AdmonCines Cine) {
        String sql = "select * from cines where active = 1 and nombre_cine = '"+ Cine.getNombreCine()+"'";
        List<String> list = new ArrayList<>();
        return list;
    }

    public Cursor DatosAEditarCine(AdmonCines Cine)
    {
        String[] campos = new String[] {id,nombreCine,direccionCine,numSal,longitudCine,latitudCine};
        String[] args = new String[] {getNombreCine()};

        Cursor matriz = db.query(tableName, campos, "nombre_cine=?", args, null, null, null);

        return matriz;
    }

    public int EditarCine (AdmonCines Cine)
    {
        try
        {
           }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }

    public int EliminarCine (AdmonCines Cine)
    {
        //En lugar de eliminar el registro de un cine, lo que se hace es actualizar el registro
        //en el campo isActive con numero diferente de 1, por estandarizacion 0.
        try
        {
             }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }


    public List<String> SelectCinesVentas(AdmonCines Cine) {
        String sql = "select * from cines where active = 1";
        List<String> list = new ArrayList<>();
        return list;
    }

    public String Espacios(String Cadena)
    {
        String espacios = " ";
        int contador = 25;
        if(Cadena.length() < 15)
        {
            contador = 30;
        }
        for (int i = Cadena.length(); i <= contador
                ; i++)
        {
            espacios = espacios + " ";
        }
        return espacios;
    }


    public String getNombreCine() {
        return NombreCine;
    }

    public void setNombreCine(String nombreCine) {
        NombreCine = nombreCine;
    }

    public String getDireccionCine() { return DireccionCine; }

    public void setDireccionCine(String direccionCine) { DireccionCine = direccionCine; }

    public int getLongitud() { return longitud; }

    public void setLongitud(int longitud) { this.longitud = longitud; }

    public int getLatitud() { return latitud; }

    public void setLatitud(int latitud) { this.latitud = latitud; }

    public int getCine_id() { return cine_id; }

    public void setCine_id(int cine_id) { this.cine_id = cine_id; }
}
