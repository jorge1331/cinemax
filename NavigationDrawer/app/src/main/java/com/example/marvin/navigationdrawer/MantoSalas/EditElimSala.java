package com.example.marvin.navigationdrawer.MantoSalas;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.marvin.navigationdrawer.AdmonElement.AdmonSalas;
import com.example.marvin.navigationdrawer.MantoCines.admonCines;
import com.example.marvin.navigationdrawer.Principal;
import com.example.marvin.navigationdrawer.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditElimSala extends Fragment
{

    Bundle captura;
    View v;
    int id_sala = 0;
    EditText nombreSala, numButacas, cineSala;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_edit_elim_sala, container, false);
        return v;
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        //Conseguimos el nombre seleccionado en el listview.. con este consultaremos la BD
        captura = getArguments();
        String nomSala = captura.getString("NombreSala");


        //llamo al metodo que me conseguira el registro con todos los campos
        conseguirSala(nomSala);


        //evaluamos que boton a sido pulsado

        Button BtnEditar = (Button)v.findViewById(R.id.btnEditarSala);
        BtnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnEditar();
            }
        });

        Button BtnBorrar = (Button)v.findViewById(R.id.btnBorrarSala);
        BtnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BorrarSala();

            }
        });

        Button BtnAtras = (Button)v.findViewById(R.id.btnAtrasSala);
        BtnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(1);
            }
        });

        Button BtnGuardar = (Button)v.findViewById(R.id.btnGuardarEditSala);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditarSala();
                PantPrincipal(1);
            }
        });

    }

    //Este metodo conseguira todos los registros para poderlos editar por el usuario
    public void conseguirSala (String nombre)
    {

        String nomSala = "", nomCine = "";
        int NumButacas = 0, idCineSala = 0, idSala = 0;


        //Creo Objeto de la clase que se comunica con la bd
        AdmonSalas Sal = new AdmonSalas(getActivity());

        //Asigno lo capturado a la variable nombre de la clase que se comunica con la bd
        //por medio del set que luego en el metodo interno de la clase referenciada
        //se usara para la consulta
        Sal.setNombreSala(nombre);

        //Inicializando un cursor
        Cursor c = Sal.DatosAEditarSala(Sal);

        c.moveToFirst();

        //Asigno cada campo del registro a variables, para luego ser utilizadas
        if (c.moveToFirst())
        {
            do {
                id_sala = c.getInt(0);
                nomSala = c.getString(1);
                NumButacas = c.getInt(2);
                idCineSala = c.getInt(3);

            }while(c.moveToNext());
        }


        //Haciendo Cast de los objetos a utilizar
        nombreSala = (EditText) v.findViewById(R.id.edtNombreSala);
        numButacas = (EditText) v.findViewById(R.id.edtNumButacas);
        cineSala = (EditText) v.findViewById(R.id.edtCineSala);


        nombreSala.setText(nomSala);
        numButacas.setText(String.valueOf(NumButacas));

        nomCine = ObtenerNombreCine(idCineSala);

        cineSala.setText(nomCine);

    }

    //Este metodo nos devuelve el nombre del cine tomanado como parametro el idCineSala.
    public String ObtenerNombreCine (int idCine)
    {
        AdmonSalas Sal = new AdmonSalas(getActivity());

        //Asigno lo capturado a la variable nombre de la clase que se comunica con la bd
        //por medio del set que luego en el metodo interno de la clase referenciada
        //se usara para la consulta
        Sal.setIdCine(idCine);

        //Inicializando un cursor
        Cursor c = Sal.GetCine(Sal);

        c.moveToFirst();

        String nCine = "";
        //Asigno cada campo del registro a variables, para luego ser utilizadas
        if (c.moveToFirst())
        {
            do {
                nCine = c.getString(0);

            }while(c.moveToNext());
        }

        return  nCine;

    }



    public void EditarSala()
    {
        int cod = id_sala;

        nombreSala = (EditText) v.findViewById(R.id.edtNombreSala);
        numButacas = (EditText) v.findViewById(R.id.edtNumButacas);
        cineSala = (EditText) v.findViewById(R.id.edtCineSala);


        if(TextUtils.isEmpty(nombreSala.getText()))
        {
            nombreSala.setFocusable(true);
            nombreSala.setError("Ingrese Nombre de Sala");
        }
        else if(TextUtils.isEmpty(numButacas.getText()))
        {
            numButacas.setFocusable(true);
            numButacas.setError("Ingrese un Numero de Butaca");
        }
        else
        {
            AdmonSalas Sal = new AdmonSalas(getActivity());

            //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
            Sal.setNombreSala(nombreSala.getText().toString());
            Sal.setNumeroButacas(Integer.parseInt(nombreSala.getText().toString()));

            int Resp = Sal.EditarSala(Sal);


            if(Resp == 1)
            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                PantPrincipal(1);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void BorrarSala()
    {
        int cod = id_sala;

        AdmonSalas Sal = new AdmonSalas(getActivity());

        //Le asigno los valores ingresados por el usuario a las variables que se van a guardar en la BD
        Sal.setIdSala(cod);

        int Resp = Sal.EliminarSala(Sal);


        if(Resp == 1)
        {
            Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
            PantPrincipal(1);
        }
        else
        {
            Toast.makeText(getActivity(), "Error al procesar la informacion", Toast.LENGTH_LONG).show();
        }
    }


    //Metodo para llamar un nuevo fragment al pulsar algun boton.
    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new Principal();
        if(FragementEle == 1)
        {
            newFragment = new admon_salas();


        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }



    //Metodo para habilitar los campos bloqueados y mostrar los ocultos para editar el campo.
    public void btnEditar()
    {
        //Asignando a cada variable un objeto de la vista a minupular
        nombreSala = (EditText) v.findViewById(R.id.edtNombreSala);
        numButacas = (EditText) v.findViewById(R.id.edtNumButacas);
        cineSala = (EditText) v.findViewById(R.id.edtCineSala);
        Button guardar = (Button)v.findViewById(R.id.btnGuardarEditSala);
        Button editar = (Button)v.findViewById(R.id.btnEditarSala);
        Button eliminar = (Button) v.findViewById(R.id.btnBorrarSala);

        //Al dar click en Editar los EditText se habilitaran para edicion
        nombreSala.setEnabled(true);
        numButacas.setEnabled(true);

        //Algunos botones se muestran y otros se ocultan
        guardar.setVisibility(View.VISIBLE);
        editar.setVisibility(View.INVISIBLE);
        eliminar.setVisibility(View.INVISIBLE);

    }

}
