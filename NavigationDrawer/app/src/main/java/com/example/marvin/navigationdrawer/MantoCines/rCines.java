/**
 * Created by Marvin on 15/03/2017.
 */
package com.example.marvin.navigationdrawer.MantoCines;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.marvin.navigationdrawer.AdmonElement.AdmonCines;
import com.example.marvin.navigationdrawer.Models.Cine;
import com.example.marvin.navigationdrawer.R;
import com.example.marvin.navigationdrawer.Tools.Constantes;
import com.example.marvin.navigationdrawer.Web.BaseBolleyFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link rCines.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link rCines#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class rCines extends Fragment {

    EditText edtNombre, edtDireccion, edtLongitud, edtLatitud;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_r_cines, container, false);

        return v;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button BtnGuardar = (Button)v.findViewById(R.id.btnGuardarCine);
        BtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               GuardarCine();

            }
        });

        Button Btn = (Button)v.findViewById(R.id.btnAtras);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PantPrincipal(1);
            }
        });
    }


    public void GuardarCine()
    {
        edtNombre = (EditText)v.findViewById(R.id.edtNombreCine);
        edtDireccion = (EditText)v.findViewById(R.id.edtDireccionCine);
        edtLongitud = (EditText)v.findViewById(R.id.edtLongitud);
        edtLatitud = (EditText)v.findViewById(R.id.edtLatitud);

        if(TextUtils.isEmpty(edtNombre.getText()))
        {
            edtNombre.setFocusable(true);
            edtNombre.setError("Ingrese cine");
        }
        else if(TextUtils.isEmpty(edtDireccion.getText()))
        {
            edtDireccion.setFocusable(true);
            edtDireccion.setError("Ingrese Direccion");
        }
        else if(TextUtils.isEmpty(edtLatitud.getText()))
        {
            edtLatitud.setFocusable(true);
            edtLatitud.setError("Ingrese Latitud");
        }
        else if(TextUtils.isEmpty(edtLongitud.getText()))
        {
            edtLongitud.setFocusable(true);
            edtLongitud.setError("Ingrese Longitud");
        }
        else
        {
            AdmonCines cine = new AdmonCines();
            Cine Cin = new Cine();
            Cin.setNombre_cine(edtNombre.getText().toString());
            Cin.setDireccion(edtDireccion.getText().toString());
            Cin.setLatitud(Integer.parseInt(edtLatitud.getText().toString()));
            Cin.setLongitud(Integer.parseInt(edtLongitud.getText().toString()));

            String Resp = NuevoCine(Cin);


            if(Resp == "correcto")
            {
                Toast.makeText(getActivity(), "Solicitud procesada correctamente", Toast.LENGTH_LONG).show();
                edtNombre.setText("");
            }
            else
            {
                Toast.makeText(getActivity(), Resp, Toast.LENGTH_LONG).show();
            }

        }
    }

    public void PantPrincipal(int FragementEle)
    {
        Fragment newFragment = new admonCines();
        if(FragementEle == 1) {
            newFragment = new admonCines();
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.content_main, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

        //getActivity().onBackPressed();
    }

    public String NuevoCine(Cine cine)
    {
        try
        {
            String url = "";
            url = Constantes.nuevo_cine;


            //int lab = spnLaboratorios.getSelectedItemPosition();
            //int Objeto = spnObjeto.getSelectedItemPosition();


            HashMap<String, String> map = new HashMap<>();// MAPEO
            map.put("nombre_cine", cine.getNombre_cine());
            map.put("direccion", cine.getDireccion());
            map.put("latitud", String.valueOf(cine.getLatitud()));
            map.put("longitud", String.valueOf(cine.getLatitud()));
            map.put("numeroSalas", String.valueOf(cine.getNumeroSalas()));
            map.put("isActive", "1");
            JSONObject jobject = new JSONObject(map);// Objeto Json

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jobject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    processRequestInsert(response);
                    onConnectionFinished();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    onConnectionFailed(volleyError.toString());
                }
            });
            addToQueue(request);
        }catch (Exception e)
        {
            return e.getMessage();
        }
        return "correcto";
    }



    private void processRequestInsert(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Registro agregado con exito",
                            Toast.LENGTH_LONG).show();
                    //  RedirectExit();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            getActivity(),
                            "Ha ocurrido un error",
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    getActivity().setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    getActivity().finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private  com.example.marvin.navigationdrawer.Web.VolleyS volley;
    protected RequestQueue fRequestQueue;




    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volley =  com.example.marvin.navigationdrawer.Web.VolleyS.getInstance(getActivity().getApplicationContext());
        fRequestQueue = volley.getRequestQueue();
    }


    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }


    public void onPreStartConnection() {
        getActivity().setProgressBarIndeterminateVisibility(true);
    }


    public void onConnectionFinished() {
        getActivity().setProgressBarIndeterminateVisibility(false);
    }


    public void onConnectionFailed(String error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }
}
