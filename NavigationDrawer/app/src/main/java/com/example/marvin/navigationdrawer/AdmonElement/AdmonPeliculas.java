package com.example.marvin.navigationdrawer.AdmonElement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.marvin.navigationdrawer.Models.Pelicula;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marvin on 15/03/2017.
 */
public class AdmonPeliculas
{

    SQLiteDatabase db;
    SQLiteManager manager;

    private String nombrePelicula, directorPelicula, sinopsisPelicula, escritorPelicula, productoraPelicula, generoPelicula, clasificacionPelicula;
    private int idPelicula, añoPelicula;

    String tableName = "peliculas";
    String id_pelicula = "id_pelicula", nombre_pelicula = "nombre_pelicula", sinopsis_pelicula = "sinopsis_pelicula";
    String director_pelicula = "director_pelicula", escritor_pelicula = "escritor_pelicula", productora_pelicula = "productora_pelicula";
    String genero_pelicula = "genero_pelicula", clasificacion_pelicula = "clasificacion_pelicula", año_pelicula = "año_pelicula";

    public AdmonPeliculas(Context context)
    {
        manager = new SQLiteManager(context, "ETPS3_AvanceCine", null, 1);
        db = manager.getWritableDatabase();
    }


    public int NuevaPelicula(Pelicula Pel)
    {
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put(nombre_pelicula, Pel.getNombre_pelicula());
            Valores.put(sinopsis_pelicula,Pel.getSipnosis_pelicula());
            Valores.put(director_pelicula,Pel.getDirector_pelicula());
            Valores.put(escritor_pelicula,Pel.getEscritor_pelicula());
            Valores.put(productora_pelicula,Pel.getProductora_pelicula());
            Valores.put(genero_pelicula,Pel.getGenero_pelicula());
            Valores.put(clasificacion_pelicula,Pel.getClasificacion_pelicula());
            Valores.put(año_pelicula,Pel.getAno_pelicula());
        }catch (Exception e)
        {
            return 0;
        }


        return 1;
    }


    public List<String> SearchPeliculas(AdmonPeliculas Pel) {
        String sql = "select * from "+tableName+" where isActive = 1 and "+nombre_pelicula+" = '"+ Pel.getNombrePelicula()+"'";
        List<String> list = new ArrayList<>();
        Cursor matriz = this.db.rawQuery(sql, null);
        while(matriz.moveToNext()) {
            list.add(matriz.getString(1).toString());
        }
        return list;
    }


    public List<String> SelectPeliculas(AdmonPeliculas Pel)
    {
        String sql = "select * from "+tableName+" where isActive = 1";
        List<String> list = new ArrayList<>();
        Cursor matriz = this.db.rawQuery(sql, null);
        while(matriz.moveToNext()) {
            list.add(matriz.getString(1).toString());
        }
        return list;
    }


    public Cursor DatosAEditarPelicula(AdmonPeliculas Pel)
    {
        String[] campos = new String[] {id_pelicula, nombre_pelicula, sinopsis_pelicula, director_pelicula, escritor_pelicula, productora_pelicula, año_pelicula, clasificacion_pelicula, genero_pelicula};
        String[] args = new String[] {getNombrePelicula()};

        Cursor matriz = db.query(tableName, campos, ""+nombre_pelicula+"=?", args, null, null, null);

        return matriz;
    }



    public int EditarPelicula (AdmonPeliculas Pel)
    {
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put(nombre_pelicula, Pel.getNombrePelicula());
            Valores.put(sinopsis_pelicula,Pel.getSinopsisPelicula());
            Valores.put(director_pelicula,Pel.getDirectorPelicula());
            Valores.put(escritor_pelicula,Pel.getEscritorPelicula());
            Valores.put(productora_pelicula,Pel.getProductoraPelicula());
            Valores.put(genero_pelicula,Pel.getGeneroPelicula());
            Valores.put(clasificacion_pelicula,Pel.getClasificacionPelicula());
            Valores.put(año_pelicula,Pel.getAñoPelicula());
            this.manager.getWritableDatabase().update(tableName,Valores,""+id_pelicula+"=?",new String[]{(String.valueOf(getIdPelicula()))});
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }

    public int EliminarPelicula (AdmonPeliculas Pel)
    {
        //En lugar de eliminar el registro de un cine, lo que se hace es actualizar el registro
        //en el campo isActive con numero diferente de 1, por estandarizacion 0.
        try
        {
            ContentValues Valores = new ContentValues();
            Valores.put("isActive", 0);

            this.manager.getWritableDatabase().update(tableName,Valores,""+id_pelicula+"=?",new String[]{(String.valueOf(getIdPelicula()))});
        }catch (Exception e)
        {
            return 0;
        }
        return 1;
    }





    public String getNombrePelicula() { return nombrePelicula; }

    public void setNombrePelicula(String nombrePelicula) { this.nombrePelicula = nombrePelicula; }

    public String getDirectorPelicula() { return directorPelicula; }

    public void setDirectorPelicula(String directorPelicula) { this.directorPelicula = directorPelicula; }

    public String getSinopsisPelicula() { return sinopsisPelicula; }

    public void setSinopsisPelicula(String sinopsisPelicula) { this.sinopsisPelicula = sinopsisPelicula; }

    public String getProductoraPelicula() { return productoraPelicula; }

    public void setProductoraPelicula(String productoraPelicula) { this.productoraPelicula = productoraPelicula; }

    public String getEscritorPelicula() { return escritorPelicula; }

    public void setEscritorPelicula(String escritorPelicula) { this.escritorPelicula = escritorPelicula; }

    public String getGeneroPelicula() { return generoPelicula; }

    public void setGeneroPelicula(String generoPelicula) { this.generoPelicula = generoPelicula; }

    public String getClasificacionPelicula() { return clasificacionPelicula; }

    public void setClasificacionPelicula(String clasificacionPelicula) { this.clasificacionPelicula = clasificacionPelicula; }

    public int getIdPelicula() { return idPelicula; }

    public void setIdPelicula(int idPelicula) { this.idPelicula = idPelicula; }

    public int getAñoPelicula() { return añoPelicula; }

    public void setAñoPelicula(int añoPelicula) { this.añoPelicula = añoPelicula; }
}
